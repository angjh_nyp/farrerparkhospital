//
//  DataAccess.swift
//  ARKitPersistence
//
//  Created by Mission on 31/10/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//
import SceneKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class DataAccess{
    let databaseRef = Database.database().reference()
    let storageRef = Storage.storage().reference()
    
    func uploadMap(name: String, data: Data, completion: @escaping (Bool) -> Void){
        let fileName = name + ".arexperience"
        let mapRef = self.storageRef.child(fileName)
        mapRef.putData(data, metadata: nil) { (metadata, error) in
            if metadata != nil{
                completion(true)
            }
            if error != nil{
                completion(false)
            }
        }
    }
    
    func saveHitPoints(name: String, points: [Point], completion: @escaping (Bool) -> Void){
        let ref = databaseRef.child("points").child(name)
        ref.setValue([])
        for p in points {
            ref.childByAutoId().setValue(
                ["type": p.type!,
                 "x": p.coordinate!.x,
                    "y": p.coordinate!.y,
                    "z": p.coordinate!.z]
            )
        }
        completion(true)
    }
    
    func getMapData(name: String, completion: @escaping (Data?) -> Void){
        let fileName = name + ".arexperience"
        let mapRef = self.storageRef.child(fileName)
        mapRef.downloadURL { url, error in
            if url != nil{
                completion(try! Data(contentsOf: url!))
            }else{
                completion(nil)
            }
        }
    }
    
    func getImages(name: String, completion: @escaping ([UIImage?]) -> Void){
        var images:[UIImage] = []
        var processedCount = 0
        let storageRef = self.storageRef.child(name);
        storageRef.listAll { (result, error) in
            result.items.forEach { (img) in
                img.getData(maxSize: 5 * 1024 * 1024) { (data, err) in
                    let image = UIImage(data: data!)
                    images.append(image!)
                    processedCount += 1
                    
                    if processedCount == result.items.count{
                        completion(images)
                    }
                }
            }
        }
    }
    
    func getAllHitPoints(name: String, completion: @escaping ([SCNVector3]) -> Void){
        var hitPoints: [SCNVector3] = []
        databaseRef.child("points").child(name).observeSingleEvent(of: .value, with: {(snapshot) in
            for record in snapshot.children{
                let r = record as! DataSnapshot
                
                let dict = r.value as! [String: Any]
                let x = dict["x"] as! CGFloat
                let y = dict["y"] as! CGFloat
                let z = dict["z"] as! CGFloat
                
                hitPoints.append(SCNVector3(x, y, z))
            }
            
            completion(hitPoints)
        })
    }
    
    func getAllPlace(completion: @escaping ([Place]) -> Void){
        var placeList : [Place] = []
        databaseRef.child("places").observeSingleEvent(of: .value, with: {(snapshot) in
            for record in snapshot.children{
                let r = record as! DataSnapshot
                let p = Place()
                
                p.id = r.key
                p.name = r.value as? String
                
                placeList.append(p)
            }
            
            completion(placeList)
        })
    }
}
