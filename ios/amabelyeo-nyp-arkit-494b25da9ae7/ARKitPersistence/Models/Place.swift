//
//  Place.swift
//  ARKitPersistence
//
//  Created by Mission on 2/11/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//

import Foundation

class Place: NSObject {
    var id: String?
    var name: String?
}
