//
//  Point.swift
//  ARKitPersistence
//
//  Created by Mission on 11/12/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//

import Foundation
import SceneKit

class Point: NSObject {
    var type: String?
    var coordinate: SCNVector3?
    var text: String?
}
