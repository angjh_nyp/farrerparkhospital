//
//  CreateRouteDetailsViewController.swift
//  ARKitPersistence
//
//  Created by Mission on 2/11/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//
import UIKit
import SceneKit
import Firebase
import FirebaseAuth

class CreateRouteDetailsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var startPicker: UIPickerView!
    @IBOutlet weak var endPicker: UIPickerView!
    
    
    // var data: Data?
    //var hitPoints: [SCNVector3]?
    
    let dataAccess = DataAccess()
    var startPickerData: [Place] = [Place]()
    var endPickerData: [Place] = [Place]()
    
    override func viewWillAppear(_ animated: Bool) {
        dataAccess.getAllPlace(completion: { (placeList) in
            self.startPickerData = placeList
            self.endPickerData = placeList
            
            self.startPicker.reloadAllComponents()
            self.endPicker.reloadAllComponents()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        
        // Connect data:
        self.startPicker.delegate = self
        self.startPicker.dataSource = self
        
        self.endPicker.delegate = self
        self.endPicker.dataSource = self
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isLoading(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    // Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectedStart = startPickerData[startPicker.selectedRow(inComponent: 0)].id
        let selectedEnd = endPickerData[endPicker.selectedRow(inComponent: 0)].id
        if selectedStart == selectedEnd{
            self.showAlert(message: "Please select different start and end points")
            return
        }
        
        if segue.destination is CreateMapViewController {
            let vc = segue.destination as! CreateMapViewController
            vc.selectedStart = selectedStart!
            vc.selectedEnd = selectedEnd!
        }
    }
    
    @IBAction func logout(_ sender: Any) {
           let firebaseAuth = Auth.auth()
                 do {
                   try firebaseAuth.signOut()
                     self.navigationController?.popViewController(animated: true)
                 } catch let signOutError as NSError {
                   print ("Error signing out: %@", signOutError)
                 }
                   
       }
    
    // Protocols
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return startPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == startPicker{
            return startPickerData[row].name
        }
        
        if pickerView == endPicker{
            return endPickerData[row].name
        }
        
        return ""
    }
    
}
