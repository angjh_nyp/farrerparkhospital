//
//  LoadMapViewController.swift
//  ARKitPersistence
//
//  Created by Mission on 2/11/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import Firebase
import FirebaseStorage

class LoadMapViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var infoLabel: UILabel!
    
    let dataAccess = DataAccess()
    var filename: String? = ""
    
    var referenceImages = Set<ARReferenceImage>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // set session delegate
        self.sceneView.session.delegate = self
        
        // Create a new scene
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene
        
        //Set lighting to the view
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration and run the view's session
        resetTrackingConfiguration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func resetTrackingConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        configuration.detectionImages = referenceImages
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.debugOptions = [.showFeaturePoints]
        sceneView.session.run(configuration, options: options)
        
        setUpInfoLabelMsg(text: "Move the camera around to detect surfaces")
    }
    
    func generateSphereNode() -> SCNNode {
        let sphere = SCNSphere(radius: 0.1)
        let sphereNode = SCNNode()
        sphereNode.geometry = sphere
        sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        
        return sphereNode
    }
    
    func generateArrowNode(name: String) -> SCNNode{
        let planeGeometry = SCNPlane(width: 0.5, height: 0.35)
        let material = SCNMaterial()
        if name == Constant.NORMAL_NODE_NAME{
            material.diffuse.contents = UIImage(named: "arrow_normal")
        } else if name == Constant.DESTINATION_NODE_NAME{
            material.diffuse.contents = UIImage(named: "arrow_end")
        }
        
        planeGeometry.materials = [material]
        
        let arrowNode = SCNNode()
        arrowNode.geometry = planeGeometry
        arrowNode.eulerAngles.x = -.pi/2
        
        return arrowNode
    }
    
   func generateTextNode(text: String) -> SCNNode{
        let text = SCNText(string: text, extrusionDepth: 0.1)
        text.font = UIFont.systemFont(ofSize: 5)
        text.flatness = 0.005
        let textNode = SCNNode(geometry: text)
        let fontScale: Float = 0.01
        textNode.scale = SCNVector3(fontScale, fontScale, fontScale)
        
        let (min, max) = (text.boundingBox.min, text.boundingBox.max)
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = min.y + 0.5 * (max.y - min.y)
        let dz = min.z + 0.5 * (max.z - min.z)
        textNode.pivot = SCNMatrix4MakeTranslation(dx, dy, dz)
        
        let width = (max.x - min.x) * fontScale
        let height = (max.y - min.y) * fontScale
        
        let plane = SCNPlane(width: CGFloat(width), height: CGFloat(height))
        let planeNode = SCNNode(geometry: plane)
        planeNode.geometry?.firstMaterial?.diffuse.contents = UIColor.green.withAlphaComponent(0.5)
        planeNode.geometry?.firstMaterial?.isDoubleSided = true
        planeNode.position = textNode.position
        textNode.eulerAngles = planeNode.eulerAngles
        planeNode.addChildNode(textNode)

        return planeNode
    }
    
    // MARK: - Dynamic Images
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(inputImage, from: inputImage.extent) {
            return cgImage
        }
        return nil
    }
    
    
    // MARK: - UI
    func setUpInfoLabelMsg(text: String){
        self.infoLabel.text = text
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isLoading(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func setupUI() {
        self.isLoading()
        self.dataAccess.getMapData(name: self.filename!) {(mapData) in
            print(self.filename!)
            
            if mapData == nil{
                self.dismiss(animated: true, completion: nil)
                self.showAlert(message: "No route available")
                return
            }
            
            print("Attempt to get images")
            self.dataAccess.getImages(name: self.filename!) { (images) in
                var count = 1
                images.forEach { (img) in
                    let imageToCIImage = CIImage(image: img!)
                    let cgImage = self.convertCIImageToCGImage(inputImage: imageToCIImage!)
                    
                    //4. Create An ARReference Image (Remembering Physical Width Is In Metres)
                    let arImage = ARReferenceImage(cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: 0.2)
                    
                    //5. Name The Image
                    arImage.name = String(count)
                    count += 1
                    
                    print("Added" + arImage.name!)
                    self.referenceImages.insert(arImage)
                }
                
                let worldMap = try! NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: mapData!)
                let configuration = ARWorldTrackingConfiguration()
                configuration.planeDetection = [.horizontal, .vertical]
                configuration.detectionImages = self.referenceImages
                
                let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
                
                self.dismiss(animated: true, completion: nil)
                
                if let worldMap = worldMap {
                    configuration.initialWorldMap = worldMap
                    self.showAlert(message: "Map Loaded")
                } else {
                    self.setUpInfoLabelMsg(text: "Move the camera around to detect surfaces")
                }
                //self.sceneView.debugOptions = [.showFeaturePoints]
                self.sceneView.session.run(configuration, options: options)
            }
        }
    }
    
    // MARK: - ARSCNViewDelegate
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        //1. If Out Target Image Has Been Detected Than Get The Corresponding Anchor
        if anchor is ARImageAnchor {
            return // Don't need to do anything in iOS
//            let x = currentImageAnchor.transform
//            print(x.columns.3.x, x.columns.3.y , x.columns.3.z)
//
//            //2. Get The Targets Name
//            let name = currentImageAnchor.referenceImage.name!
//
//            //3. Get The Targets Width & Height In Meters
//            let width = currentImageAnchor.referenceImage.physicalSize.width
//            let height = currentImageAnchor.referenceImage.physicalSize.height
//
//            print("""
//                Image Name = \(name)
//                Image Width = \(width)
//                Image Height = \(height)
//                """)
            
//            //4. Create A Plane Geometry To Cover The ARImageAnchor
//            let planeNode = SCNNode()
//            let planeGeometry = SCNPlane(width: width, height: height)
//            //planeGeometry.firstMaterial?.diffuse.contents = UIColor.white
//            planeNode.opacity = 0
//            planeNode.geometry = planeGeometry
//
//            //5. Rotate The PlaneNode To Horizontal
//            planeNode.eulerAngles.x = -.pi/2
            
            //The Node Is Centered In The Anchor (0,0,0)
            //node.addChildNode(planeNode)
            
            //6. Create AN SCNBox
//            let sphereNode = generateSphereNode()
//            //9. Set The Boxes Position To Be Placed On The Plane (node.x + box.height)
//            sphereNode.position = SCNVector3(0 , 0.05, 0)
//
//            //10. Add The Box To The Node
//            node.addChildNode(sphereNode)
        }
        
        if let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.alignment == .vertical,
            let geom = ARSCNPlaneGeometry(device: MTLCreateSystemDefaultDevice()!)
        {
            geom.update(from: planeAnchor.geometry)
            geom.firstMaterial?.colorBufferWriteMask = .alpha
            node.geometry = geom
        }
        
        guard !(anchor is ARPlaneAnchor) else { return }
        
        if anchor.name! != Constant.FIRST_NODE_NAME{
            var anchorNode: SCNNode;
            if anchor.name! == Constant.NORMAL_NODE_NAME || anchor.name! == Constant.DESTINATION_NODE_NAME{
                anchorNode = generateArrowNode(name: anchor.name!)
            }else{
                anchorNode = generateTextNode(text: anchor.name!)
            }
            
            DispatchQueue.main.async {
                node.addChildNode(anchorNode)
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.alignment == .vertical,
            let geom = node.geometry as? ARSCNPlaneGeometry
        {
            geom.update(from: planeAnchor.geometry)
        }
    }
    
    // MARK: - ARSessionDelegate
    //shows the current status of the world map.
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        switch frame.worldMappingStatus {
        case .notAvailable:
            setUpInfoLabelMsg(text: "Map Status: Not available")
        case .limited:
            setUpInfoLabelMsg(text: "Map Status: Available but has Limited features")
        case .extending:
            setUpInfoLabelMsg(text: "Map Status: Actively extending the map")
        case .mapped:
            setUpInfoLabelMsg(text: "Map Status: Points Loaded")
        }
    }
}
