//
//  LoadMapNoQRFormViewController.swift
//  ARKitPersistence
//
//  Created by Mission on 19/11/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//

import UIKit

class LoadMapFormNoQRViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {
    @IBOutlet weak var endPicker: UIPickerView!
    @IBOutlet weak var startPicker: UIPickerView!
    
    let dataAccess = DataAccess()
    var startPickerData: [Place] = [Place]()
    var endPickerData: [Place] = [Place]()
    
    override func viewWillAppear(_ animated: Bool) {
        dataAccess.getAllPlace(completion: { (placeList) in
            self.startPickerData = placeList
            self.endPickerData = placeList
            
            self.startPicker.reloadAllComponents()
            self.endPicker.reloadAllComponents()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startPicker.delegate = self
        self.endPicker.dataSource = self
        self.endPicker.delegate = self
        self.endPicker.dataSource = self
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func confirmButtonAction(_ sender: Any) {
        let selectedStart = startPickerData[startPicker.selectedRow(inComponent: 0)].id
        let selectedEnd = endPickerData[endPicker.selectedRow(inComponent: 0)].id
        let filename = "S" + selectedStart! + "E" + selectedEnd!
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ShowMapStoryboard") as! LoadMapViewController
        vc.filename = filename
        
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    // Protocols
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == startPicker{
            return startPickerData.count
        }
        if pickerView == endPicker{
            return endPickerData.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == startPicker{
            return startPickerData[row].name
        }
        
        if pickerView == endPicker{
            return endPickerData[row].name
        }
        
        return ""
    }
    
}
