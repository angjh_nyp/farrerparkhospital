//
//  ViewController.swift
//  ARKitPersistence
//
//  Created by Ananth Bhamidipati on 25/10/2018.
//  Copyright © 2018 YellowJersey. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import FocusNode
import SmartHitTest
import Firebase
import FirebaseStorage

extension ARSCNView: ARSmartHitTest {}

class CreateMapViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var destinationSwitch: UISwitch!
    
    
    // From VC
    var filename: String = ""
    var selectedStart: String = ""
    var selectedEnd: String = ""
    
    // For current scene
    let focusSquare = FocusSquare()
    
    var hitPoints = [Point]()
    var anchors = [ARAnchor]()
    
    //var isFirstAnchor = true
    var lastAnchorAdded = false
    
    // edit mode enabled
    var addNodeMode = true
    var textToAdd: String = ""
    
    var referenceImages = Set<ARReferenceImage>()
    
    // Database
    let dataAccess = DataAccess()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // set session delegate
        self.sceneView.session.delegate = self
        
        // Create a new scene
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene
        
        //Set lighting to the view
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
        
        // Set filename
        filename = "S" + selectedStart + "E" + selectedEnd
        
        // load images
        loadImages()
        
        // Focus node
        self.focusSquare.viewDelegate = sceneView
        sceneView.scene.rootNode.addChildNode(self.focusSquare)
        
        setupUI()
        addTapGestureRecognizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration and run the view's session
        resetTrackingConfiguration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    func loadImages(){
        self.dataAccess.getImages(name: self.filename) { (images) in
            var count = 1
            images.forEach { (img) in
                let imageToCIImage = CIImage(image: img!)
                let cgImage = self.convertCIImageToCGImage(inputImage: imageToCIImage!)
                
                //4. Create An ARReference Image (Remembering Physical Width Is In Metres)
                let arImage = ARReferenceImage(cgImage!, orientation: CGImagePropertyOrientation.up, physicalWidth: 0.2)
                
                //5. Name The Image
                arImage.name = String(count)
                count += 1
                
                print("Added " + arImage.name!)
                self.referenceImages.insert(arImage)
            }
            self.resetTrackingConfiguration()
        }
    }
    
    func resetTrackingConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        configuration.detectionImages = referenceImages
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        //sceneView.debugOptions = [.showFeaturePoints]
        sceneView.session.run(configuration, options: options)
        
        destinationSwitch.isOn = false
        
        self.lastAnchorAdded = false
        self.anchors = []
        self.hitPoints = []
        setUpLabelsAndButtons(text: "Move the camera around to detect surfaces", canShowSaveButton: false)
    }
    
    func generateSphereNode() -> SCNNode {
        let sphere = SCNSphere(radius: 0.1)
        let sphereNode = SCNNode()
        sphereNode.geometry = sphere
        sphereNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        
        return sphereNode
    }
    
    func generateArrowNode(name: String) -> SCNNode{
        let planeGeometry = SCNPlane(width: 0.5, height: 0.35)
        let material = SCNMaterial()
        if name.contains(Constant.NORMAL_NODE_NAME){
            material.diffuse.contents = UIImage(named: "arrow_normal")
        }else{
            material.diffuse.contents = UIImage(named: "arrow_end")
        }
        
        planeGeometry.materials = [material]
        
        let arrowNode = SCNNode()
        arrowNode.geometry = planeGeometry
        arrowNode.eulerAngles.x = -.pi/2
        
        return arrowNode
    }
    
    func generateTextNode(text: String) -> SCNNode{
        let text = SCNText(string: text, extrusionDepth: 0.1)
        text.font = UIFont.systemFont(ofSize: 5)
        text.flatness = 0.005
        let textNode = SCNNode(geometry: text)
        let fontScale: Float = 0.01
        textNode.scale = SCNVector3(fontScale, fontScale, fontScale)
        
        let (min, max) = (text.boundingBox.min, text.boundingBox.max)
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = min.y + 0.5 * (max.y - min.y)
        let dz = min.z + 0.5 * (max.z - min.z)
        textNode.pivot = SCNMatrix4MakeTranslation(dx, dy, dz)
        
        let width = (max.x - min.x) * fontScale
        let height = (max.y - min.y) * fontScale
        
        let plane = SCNPlane(width: CGFloat(width), height: CGFloat(height))
        let planeNode = SCNNode(geometry: plane)
        planeNode.geometry?.firstMaterial?.diffuse.contents = UIColor.green.withAlphaComponent(0.5)
        planeNode.geometry?.firstMaterial?.isDoubleSided = true
        planeNode.position = textNode.position
        textNode.eulerAngles = planeNode.eulerAngles
        planeNode.addChildNode(textNode)
        
        return planeNode
    }
    
    // MARK: Touch Recognition
    func addTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognized))
        self.sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func tapGestureRecognized(recognizer :UITapGestureRecognizer) {
        guard recognizer.state == .ended else{
            return
        }
        
        if addNodeMode == false{
            let location: CGPoint = recognizer.location(in: sceneView)
            let hits = self.sceneView.hitTest(location, options: nil)
            if !hits.isEmpty{
                let tappedNode = hits.first?.node
                
                // Ask user for input
                //1. Create the alert controller.
                let alert = UIAlertController(title: "Add description", message: "Enter a description", preferredStyle: .alert)
                
                //2. Add the text field. You can configure it however you need.
                alert.addTextField { (textField) in
                    textField.text = ""
                }
                
                // 3. Grab the value from the text field, and print it when the user clicks OK.
                alert.addAction(UIAlertAction(title: "Cancel", style: .default) { (alertAction) in })
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                    let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                    self.textToAdd = textField!.text!
                    
                    // Got the tapped node, now add some label above them
                    let anchor = ARAnchor(name: self.textToAdd, transform: tappedNode!.simdWorldTransform)
                    self.anchors.append(anchor)
                    
                    let p = Point()
                    p.type = Constant.TEXT_NODE_NAME
                    p.coordinate = tappedNode!.worldPosition
                    p.text = self.textToAdd
                    self.hitPoints.append(p)
                    
                    self.sceneView.session.add(anchor: anchor)
                }))
                
                // 4. Present the alert.
                self.present(alert, animated: true, completion: nil)
            }
            return
        }
        
        // For adding nodes => addNodeMode == true
        if self.focusSquare.state != .initializing{
            var name = Constant.NORMAL_NODE_NAME
            
            if destinationSwitch.isOn{
                name = Constant.DESTINATION_NODE_NAME
                lastAnchorAdded = true
            }
            
            
            let anchor = ARAnchor(name: "\(nodeCount)\(name)", transform: self.focusSquare.simdWorldTransform)
            
            self.anchors.append(anchor)
            
            let p = Point()
            p.type = name
            p.coordinate = self.focusSquare.position
            self.hitPoints.append(p)
            
            print("""
                Position = \(self.focusSquare.position)
                World position = \(self.focusSquare.worldPosition)
                """)
            
            sceneView.session.add(anchor: anchor)
        }
    }
    
    // MARK: - Dynamic Images
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(inputImage, from: inputImage.extent) {
            return cgImage
        }
        return nil
    }
    
    
    // MARK: - UI
    func setupUI() {
        setUpLabelsAndButtons(text: "Move the camera around to detect surfaces", canShowSaveButton: false)
        resetButton.layer.cornerRadius = 10
        undoButton.layer.cornerRadius = 10
        textButton.layer.cornerRadius = 10
        saveButton.layer.cornerRadius = 10
        
        resetButton.titleLabel?.textAlignment = .center
        undoButton.titleLabel?.textAlignment = .center
        textButton.titleLabel?.textAlignment = .center
        saveButton.titleLabel?.textAlignment = .center
        destinationSwitch.isOn = false
    }
    
    func setUpLabelsAndButtons(text: String, canShowSaveButton: Bool) {
        self.infoLabel.text = text
        self.saveButton.isEnabled = canShowSaveButton
        
        if(self.anchors.count > 0){
            self.undoButton.isEnabled = true
            self.textButton.isEnabled = true
        }else{
            self.undoButton.isEnabled = false
            self.textButton.isEnabled = false
        }
        
        if !canShowSaveButton || self.anchors.count == 0{
            return
        }
        
        if !addNodeMode {
            self.infoLabel.text = "Tap an existing waypoint to add text"
        }else{
            self.infoLabel.text = "Start plotting waypoints"
        }
        
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isLoading(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Button Actions
    @IBAction func resetButtonAction(_ sender: Any) {
        resetTrackingConfiguration()
    }
    
    @IBAction func undoButtonAction(_ sender: Any) {
        self.hitPoints.removeLast()
        
        let lastAddedNode = self.anchors.popLast()
        self.sceneView.session.remove(anchor: lastAddedNode!)
        nodeCount -= 1
    }
    
    @IBAction func addTextButtonAction(_ sender: Any) {
        addNodeMode = !addNodeMode
        
        if addNodeMode == false{
            self.focusSquare.isHidden = true
        }else{
            self.focusSquare.isHidden = false
        }
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if !lastAnchorAdded {
            self.showAlert(message: "Please add a destination anchor")
            return
        }
        
        sceneView.session.getCurrentWorldMap { (worldMap, error) in
            guard let worldMap = worldMap else {
                self.setUpLabelsAndButtons(text: "Can't get current world map", canShowSaveButton: false)
                self.showAlert(message: error!.localizedDescription)
                return
            }
            
            do{
                let data = try NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true)
                self.isLoading()
                
                var mapSuccess = false
                var pointsSuccess = false
                
                self.dataAccess.uploadMap(name: self.filename, data: data){ (success) in
                    mapSuccess = success
                    
                    self.dataAccess.saveHitPoints(name: self.filename, points: self.hitPoints){ (hitSuccess) in
                        pointsSuccess = hitSuccess
                        self.dismiss(animated: true, completion: nil)
                        
                        if mapSuccess && pointsSuccess {
                            self.showAlert(message: "Map Saved")
                        }else{
                            self.showAlert(message: "Unable to save map")
                        }
                    }
                }
                
            } catch{
                fatalError("Can't save map: \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - ARSCNViewDelegate
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            self.focusSquare.updateFocusNode()
        }
    }
    
    var nodeCount = 1
    
    //this renderer is called after add nodes to scene view when the sceneview tap recognizer is called and hitpoints exist
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        //1. If Out Target Image Has Been Detected Than Get The Corresponding Anchor
        if let currentImageAnchor = anchor as? ARImageAnchor {
            let x = currentImageAnchor.transform
            print(x.columns.3.x, x.columns.3.y , x.columns.3.z)
            
            self.anchors.append(currentImageAnchor)
            let p = Point()
            p.type = Constant.IMAGE_ANCHOR_NAME
            p.coordinate = SCNVector3(x.columns.3.x, x.columns.3.y, x.columns.3.z)
            self.hitPoints.append(p)
            
            //2. Get The Targets Name
            let name = currentImageAnchor.referenceImage.name!
            
            //3. Get The Targets Width & Height In Meters
            let width = currentImageAnchor.referenceImage.physicalSize.width
            let height = currentImageAnchor.referenceImage.physicalSize.height
            
            print("""
                Image Name = \(name)
                Image Width = \(width)
                Image Height = \(height)
                """)
            
            //4. Create A Plane Geometry To Cover The ARImageAnchor
            let planeNode = SCNNode()
            let planeGeometry = SCNPlane(width: width, height: height)
            planeGeometry.firstMaterial?.diffuse.contents = UIColor.white
            planeNode.geometry = planeGeometry
            planeNode.opacity = 0
            
            //5. Rotate The PlaneNode To Horizontal
            planeNode.eulerAngles.x = -.pi/2
            
            //The Node Is Centered In The Anchor (0,0,0)
            node.addChildNode(planeNode)
            
            //6. Create AN SCNBox
            let sphereNode = generateSphereNode()
            //9. Set The Boxes Position To Be Placed On The Plane (node.x + box.height)
            sphereNode.position = SCNVector3(0 , 0.05, 0)
            
            //10. Add The Box To The Node
            node.addChildNode(sphereNode)
            
            return
        }
        
        if let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.alignment == .vertical,
            let geom = ARSCNPlaneGeometry(device: MTLCreateSystemDefaultDevice()!)
        {
            geom.update(from: planeAnchor.geometry)
            geom.firstMaterial?.colorBufferWriteMask = .alpha
            node.geometry = geom
        }
        
        guard !(anchor is ARPlaneAnchor) else { return }
        var newNode = SCNNode()
        
        if anchor.name!.contains(Constant.NORMAL_NODE_NAME) || anchor.name!.contains(Constant.DESTINATION_NODE_NAME){
            newNode = generateArrowNode(name: "\(anchor.name!)")
            nodeCount += 1
        }else{
            newNode = generateTextNode(text: self.textToAdd)
        }
        
        DispatchQueue.main.async {
            node.addChildNode(newNode)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.alignment == .vertical,
            let geom = node.geometry as? ARSCNPlaneGeometry
        {
            geom.update(from: planeAnchor.geometry)
        }
    }
    
    // MARK: - ARSessionDelegate
    //shows the current status of the world map.
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        switch frame.worldMappingStatus {
        case .notAvailable:
            setUpLabelsAndButtons(text: "Map Status: Not available", canShowSaveButton: false)
        case .limited:
            setUpLabelsAndButtons(text: "Map Status: Available but has Limited features", canShowSaveButton: true)
        case .extending:
            setUpLabelsAndButtons(text: "Map Status: Actively extending the map", canShowSaveButton: true)
        case .mapped:
            setUpLabelsAndButtons(text: "Map Status: Mapped the visible Area. Start dropping waypoints", canShowSaveButton: true)
        }
    }
}
