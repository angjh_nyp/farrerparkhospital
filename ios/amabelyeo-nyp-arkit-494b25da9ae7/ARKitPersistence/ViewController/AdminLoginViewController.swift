//
//  AdminLoginViewController.swift
//  ARKitPersistence
//
//  Created by Mission on 13/1/20.
//  Copyright © 2020 YellowJersey. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class AdminLoginViewController: UIViewController {
    @IBOutlet weak var emailTb: UITextField!
    @IBOutlet weak var passwordTb: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func login(_ sender: Any) {
        self.emailTb.text = "admin@gmail.com"
        self.passwordTb.text = "P@ssw0rd"
        guard let email = self.emailTb.text, let password = self.passwordTb.text else {
            self.showAlert(message: "Email/Password can't be empty")
          return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
           if let error = error {
             self?.showAlert(message: error.localizedDescription)
             return
            }
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "RouteDetails") as! CreateRouteDetailsViewController
           self?.navigationController!.pushViewController(vc, animated: true)
        }
    }
}
