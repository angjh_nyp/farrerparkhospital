//
//  LoadMapFormViewController.swift
//  ARKitPersistence
//
//  Created by Mission on 31/10/19.
//  Copyright © 2019 YellowJersey. All rights reserved.
//

import UIKit

class LoadMapFormViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {
    @IBOutlet weak var endPicker: UIPickerView!
    
    let dataAccess = DataAccess()
    var endPickerData: [Place] = [Place]()
    
    override func viewWillAppear(_ animated: Bool) {
        dataAccess.getAllPlace(completion: { (placeList) in
            self.endPickerData = placeList
            
            self.endPicker.reloadAllComponents()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.endPicker.delegate = self
        self.endPicker.dataSource = self
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectedEnd = endPickerData[endPicker.selectedRow(inComponent: 0)].id
        
        if segue.destination is QRScannerViewController {
            let vc = segue.destination as! QRScannerViewController
            vc.endPt = selectedEnd!
        }
    }
    
    // Protocols
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return endPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == endPicker{
            return endPickerData[row].name
        }
        
        return ""
    }
    
}
