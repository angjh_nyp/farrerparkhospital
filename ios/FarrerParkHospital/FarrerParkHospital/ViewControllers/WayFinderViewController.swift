//
//  WayFinderViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 15/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit

//var WFStartArr:[String] = ["SIT Front Lift Level 3", "SIT Back Lift Level 3", "FYP Room 335", "FYP Room 310", "SIT Admin Office", "SIT L602", "SBM B 303D"]
//var WFDestArr:[String] = ["SIT Front Lift Level 3", "SIT Back Lift Level 3", "FYP Room 335", "FYP Room 310", "SIT Admin Office", "SIT L602", "SBM B 303D"]

class WayFinderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var firstSelect:Bool = true
    var WFSelectedStart:String?
    var WFSelectedStartInd:Int?
    var WFSelectedDest:String?
    var WFSelectedDestInd:Int?
    
    let dataAccess = DataAccess()
    var endTableData: [Place] = [Place]()
    var startTableData: [Place] = [Place]()
    
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBAction func submitBtnPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        if StartTableView.indexPathForSelectedRow == nil
        {
            alert.title = "Please select start point"
            present(alert, animated: true, completion: nil)
        } else if DestTableView.indexPathForSelectedRow == nil
        {
            alert.title = "Please select destination"
            present(alert, animated: true, completion: nil)
        } else if startTableData[StartTableView.indexPathForSelectedRow!.row] == endTableData[DestTableView.indexPathForSelectedRow!.row]
        {
            alert.title = "Please select different start and destination point"
            present(alert, animated: true, completion: nil)
        } else {
            let selectedStart = startTableData[StartTableView.indexPathForSelectedRow!.row].id
            let selectedEnd =  endTableData[DestTableView.indexPathForSelectedRow!.row].id
            let filename = "S" + selectedStart! + "E" + selectedEnd!
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ShowMapStoryboard") as! LoadMapViewController
            vc.filename = filename
            self.navigationController!.pushViewController(vc, animated: true)
        }
    }
    
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBAction func homeBtnPressed(_ sender: Any) {
        menu.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    @IBOutlet weak var StartBtn: UIButton!
    @IBAction func StartBtnPressed(_ sender: UIButton) {
//        if (!StartTableView.isHidden)
//        {
//            StartTableView.isHidden = true
//        } else {
//            StartTableView.isHidden = false
//        }
    }
    @IBOutlet weak var DestBtn: UIButton!
    @IBAction func DestBtnPressed(_ sender: UIButton) {
//        if (!DestTableView.isHidden)
//        {
//            DestTableView.isHidden = true
//        } else {
//            DestTableView.isHidden = false
//        }
    }
    
    @IBOutlet weak var StartTableView: UITableView!
    @IBOutlet weak var DestTableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == StartTableView
        {
            return startTableData.count
        } else {
            return endTableData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == StartTableView
        {
            let title = startTableData[indexPath.row].name
            let cell = tableView.dequeueReusableCell(withIdentifier: "startCell")
            cell?.textLabel?.text = title
            //Yusuf:This is for the flashing of the scrollbar
            StartTableView.flashScrollIndicators()
            return cell!
        } else {
            let title = endTableData[indexPath.row].name
            let cell = tableView.dequeueReusableCell(withIdentifier: "destCell")
            cell?.textLabel?.text = title
            //Yusuf:This is for the flashing of the scrollbar
            DestTableView.flashScrollIndicators()
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == StartTableView
//        {
//
//            //if WFDestArr count is lesser than WFStartArr count, it means that starting point has been selected before
//            if (WFSelectedStart != nil)
//            {
//                var endNameData:[String] = []
//                for placeObj in endTableData
//                {
//                    endNameData.append(placeObj.name!)
//                }
//                if (!endNameData.contains(WFSelectedStart!))
//                {
//                    //and that item is removed from WFDestArr, thus, need insert back
//                    let insertPlace = Place()
//                    insertPlace.name = WFSelectedStart
//                    insertPlace.id = String(WFSelectedDestInd!)
//                    endTableData.insert(insertPlace, at: WFSelectedDestInd!)
//                }
//            }
//
//            //set new selected item for WFSelectedStart and also the index of that item in WFDestArr
//            //so that item can be inserted if another start item is selected
//
//            WFSelectedStart = startTableData[indexPath.row].name
//            for placeObj in endTableData
//            {
//                if placeObj.id! == startTableData[indexPath.row].id!
//                {
//                    WFSelectedDestInd = endTableData.firstIndex(of: placeObj)
//                }
//            }
//            endTableData.remove(at: WFSelectedDestInd!)
//            DestTableView.reloadData()
//            print(endTableData.count)
//        } else {
//            if (WFSelectedDest != nil)
//            {
//                var startNameData:[String] = []
//                for placeObj in startTableData
//                {
//                    startNameData.append(placeObj.name!)
//                }
//                if (!startNameData.contains(WFSelectedDest!))
//                {
//                    let insertPlace = Place()
//                    insertPlace.name = WFSelectedDest
//                    insertPlace.id = String(WFSelectedStartInd!)
//                    startTableData.insert(insertPlace, at: WFSelectedStartInd!)
//                }
//            }
//
//            WFSelectedDest = endTableData[indexPath.row].name
//            for placeObj in startTableData
//            {
//                if placeObj.id! == endTableData[indexPath.row].id!
//                {
//                    WFSelectedStartInd = startTableData.firstIndex(of: placeObj)
//                }
//            }
//            startTableData.remove(at: WFSelectedStartInd!)
//            StartTableView.reloadData()
//            print(startTableData.count)
//        }
        
        
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        versionLbl.isHidden = true
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        
        dataAccess.getAllPlace(completion: { (placeList) in
            self.startTableData = placeList
            self.endTableData = placeList
            
            self.StartTableView.reloadData()
            self.DestTableView.reloadData()
        })
        
        menu.currVC = self
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
        StartTableView.isHidden = false
        DestTableView.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
