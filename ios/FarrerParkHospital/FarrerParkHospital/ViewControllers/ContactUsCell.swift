//
//  ContactUsCell.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 21/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit

class ContactUsCell: UITableViewCell {
    
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var contactDesc: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
