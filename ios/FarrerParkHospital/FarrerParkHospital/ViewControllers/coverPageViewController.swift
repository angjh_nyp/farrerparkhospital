//
//  coverPageViewController.swift
//  projProto
//
//  Created by FYPJ on 27/11/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import UIKit
import SystemConfiguration
import Network

var showFlower:Bool = false
var facilityArr:[FacilityClass] = []
var gadgetArr:[GadgetClass] = []
var launchBefore:Bool = false

class coverPageViewController: UIViewController {
    
    @IBOutlet weak var homeBtn: bigCircleBtn!
    
    @IBOutlet weak var VRerrLbl: UILabel!
    
    @IBOutlet weak var versionLbl: UILabel!
    
    var slCenter: CGPoint!
    var wfCenter: CGPoint!
    var pjCenter: CGPoint!
    var erCenter: CGPoint!
    var auCenter: CGPoint!
    var cuCenter: CGPoint!
    var homeCenter: CGPoint!
    
    let menu = flowerMenu()
    var target: CGPoint!
    @IBOutlet weak var blurredScreen: ViewQualities!
    
    @IBAction func homePressed(_ sender: Any) {
        menu.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    //Check if phone is connected to network
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    var anim: CABasicAnimation?
    var activityIndicator:ActivityIndicator?
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        activityIndicator = ActivityIndicator(view: view)
        activityIndicator!.showActivityIndicator()
        //remove before deployment
        activityIndicator?.stopActivityIndicator()
        
        launchBefore = isAppAlreadyLaunchedOnce()
        
        //Only start JSON API retrieval when phone is connected to network
        if (!connectedToNetwork())
        {
            activityIndicator!.loadingTextLabel.text = "No connection"
            timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        } else {
            vrErrLblGbl = VRerrLbl
            
            UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse, .allowUserInteraction, .curveEaseOut], animations: {
                self.homeBtn.center = CGPoint(x: self.homeBtn.center.x, y: self.homeBtn.center.y+20)
            }, completion: nil)
            
            self.retrieveFacilties(completion: {
                _ in
                self.retrieveGadgets(completion: {
                    _ in
                    DispatchQueue.main.async {
                        self.activityIndicator!.stopActivityIndicator()
                    }
                })
            })
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    @objc func update()
    {
        if(!connectedToNetwork()){
            print("No connection")
            activityIndicator!.loadingTextLabel.text = "No connection"
        } else {
            print("Connected")
            activityIndicator!.loadingTextLabel.text = "Please wait while we download the resources for you"
            
            vrErrLblGbl = VRerrLbl
            
            UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse, .allowUserInteraction, .curveEaseOut], animations: {
                self.homeBtn.center = CGPoint(x: self.homeBtn.center.x, y: self.homeBtn.center.y+20)
            }, completion: nil)
            
            self.retrieveFacilties(completion: {
                _ in
                self.retrieveGadgets(completion: {
                    _ in
                    DispatchQueue.main.async {
                        self.activityIndicator!.stopActivityIndicator()
                    }
                })
            })
            timer!.invalidate()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        menu.currVC = self
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        versionLbl.isHidden = true
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //        slCenter = self.selectLocBtn.center
        //        wfCenter = self.wayFindBtn.center
        //        pjCenter = self.patJourBtn.center
        //        erCenter = self.expRealBtn.center
        //        auCenter = self.aboutUsBtn.center
        //        cuCenter = self.conUsBtn.center
        //
        //        if !showFlower
        //        {
        //            self.selectLocBtn.center = self.selectLocBtn.center
        //            self.wayFindBtn.center = self.selectLocBtn.center
        //            self.patJourBtn.center = self.selectLocBtn.center
        //            self.expRealBtn.center = self.selectLocBtn.center
        //            self.aboutUsBtn.center = self.selectLocBtn.center
        //            self.conUsBtn.center = self.selectLocBtn.center
        //        }
        //menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
    }
    var facilityObj:FacilityClass?
    var gadgetObj:GadgetClass?
    
    //activity indicator
    struct ActivityIndicator {
        let viewForActivityIndicator = UIView()
        let view: UIView
        let activityIndicatorView = UIActivityIndicatorView()
        let loadingTextLabel = UILabel()
        func showActivityIndicator() {
            viewForActivityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            viewForActivityIndicator.backgroundColor = UIColor.white
            view.addSubview(viewForActivityIndicator)
            activityIndicatorView.center = CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height / 2.0)
            loadingTextLabel.textColor = UIColor.black
            loadingTextLabel.numberOfLines = 0
            loadingTextLabel.text = "Please wait while we download the resources for you"
            loadingTextLabel.font = UIFont(name: "Titillium", size: 15)
            loadingTextLabel.sizeToFit()
            loadingTextLabel.center = CGPoint(x: activityIndicatorView.center.x, y: activityIndicatorView.center.y + 30)
            loadingTextLabel.textAlignment = .center
            viewForActivityIndicator.addSubview(loadingTextLabel)
            activityIndicatorView.hidesWhenStopped = true
            activityIndicatorView.style = .gray
            viewForActivityIndicator.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
        }
        func stopActivityIndicator() {
            viewForActivityIndicator.removeFromSuperview()
            activityIndicatorView.stopAnimating()
            activityIndicatorView.removeFromSuperview()
        }
    }
    
    //Check if app is launched for the first time
    func isAppAlreadyLaunchedOnce()->Bool{
        let defaults = UserDefaults.standard
        if let _ = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            print("App already launched")
            return true
        }
        else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            print("App launched first time")
            return false
        }
    }
    
    //add facility to document directory
    func addFacilityToDocumentDirectory(ddfPath:String, facObj:FacilityClass){
        
        print("UPDATE")
        
        // get the documents directory url
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let originalURL = documentsDirectory.appendingPathComponent(ddfPath)
        try? FileManager.default.removeItem(at: originalURL)
        
        try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(ddfPath), withIntermediateDirectories: true)
        
        //Todo check Info.plist exists
        if (checkDocumentDirectory(ddfPath: ddfPath))
        {
            let fileURL = documentsDirectory.appendingPathComponent(ddfPath).appendingPathComponent("Info.plist")
            
            do {
                // writes the image data to disk
                try FileManager.default.removeItem(at: fileURL)
                
            } catch {
                print("error removing file:", error)
            }
        }
        
        
        let dictWr:NSMutableDictionary = NSMutableDictionary.init()
        //saving values
        var facNameObj = FACILITY()
        dictWr.setObject(facObj.id , forKey: facNameObj.id as NSCopying)
        dictWr.setObject(facObj.name , forKey: facNameObj.name as NSCopying)
        dictWr.setObject(facObj.entityType , forKey: facNameObj.entityType as NSCopying)
        dictWr.setObject(facObj.vrImageLink , forKey: facNameObj.VRImage as NSCopying)
        dictWr.setObject(facObj.floorlevel , forKey: facNameObj.floorLevel as NSCopying)
        dictWr.setObject(facObj.timeStamp , forKey: facNameObj.lastModified as NSCopying)
        dictWr.setObject(facObj.hotspotsArr, forKey: facNameObj.hotspots as NSCopying)
        dictWr.setObject(facObj.vrThumbNail, forKey: facNameObj.thumbNail as NSCopying)
        
        
        //writing to Info.plist
        dictWr.write(toFile: documentsDirectory.appendingPathComponent(ddfPath + "/Info.plist").path, atomically: false)
        
        if facObj.vrImageLink != ""
        {
            //write vr image to folder
            var image:UIImage?
            let urlImage = URL(string: facObj.vrImageLink!)
            var imageURL:URL?
            let data = try? Data(contentsOf: urlImage!)
            image = UIImage(data: data!)
            let imageData = image!.jpegData(compressionQuality: 1.0)
            imageURL = documentsDirectory.appendingPathComponent("\(ddfPath)/\(facObj.name!).jpg")
            try? imageData?.write(to: imageURL!)
            print(imageURL!.path)
        } else {
            //write vr image to folder
            var imageURL:URL?
            var image = UIImage(named: "no-image")
            let imageData = image!.jpegData(compressionQuality: 1.0)
            imageURL = documentsDirectory.appendingPathComponent("\(ddfPath)/\(facObj.name!).jpg")
            try? imageData?.write(to: imageURL!)
            //print(imageURL!.path)
        }
        
        //write vr hotspot image to folder
        for x in 0..<facObj.hotspotsArr!.count
        {
            let parentPath = "\(ddfPath)/subFacility/\(facObj.hotspotsName![x])"
            let subFacilityPath = documentsDirectory.appendingPathComponent(parentPath)
            if !FileManager.default.fileExists(atPath: subFacilityPath.path)
            {
                try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(parentPath), withIntermediateDirectories: true)
            }
            let hotspot = facObj.hotspotsArr![x]
            if hotspot != ""
            {
                var hsImage:UIImage?
                let hsUrlImage = URL(string: hotspot)
                var hsImageURL:URL?
                let hsData = try? Data(contentsOf: hsUrlImage!)
                hsImage = UIImage(data: hsData!)
                let hsImageData = hsImage!.jpegData(compressionQuality: 1.0)
                hsImageURL = documentsDirectory.appendingPathComponent("\(parentPath)/hotspot.jpg")
                try? hsImageData?.write(to: hsImageURL!)
                //print(hsImageURL!.path)
            } else {
                var hsImage = UIImage(named: "no-image")
                let hsImageData = hsImage!.jpegData(compressionQuality: 1.0)
                var hsImageURL = documentsDirectory.appendingPathComponent("\(parentPath)/hotspot.jpg")
                try? hsImageData?.write(to: hsImageURL)
                //print(hsImageURL!.path)
            }
        }
        
        if facObj.vrThumbNail != ""
        {
            //write vr thumbNail image to folder
            var thumbNailImg:UIImage?
            let thumbURLpath = URL(string: facObj.vrThumbNail!)
            var thumbURL:URL?
            let thumbData = try? Data(contentsOf: thumbURLpath!)
            thumbNailImg = UIImage(data: thumbData!)
            let thumbImageData = thumbNailImg!.jpegData(compressionQuality: 1.0)
            thumbURL = documentsDirectory.appendingPathComponent("\(ddfPath)/\(facObj.name!) Thumb.jpg")
            try? thumbImageData?.write(to: thumbURL!)
        } else {
            //write vr thumbNail image to folder
            var thumbURL:URL?
            var thumbNailImg = UIImage(named: "no-image")
            let thumbImageData = thumbNailImg!.jpegData(compressionQuality: 1.0)
            thumbURL = documentsDirectory.appendingPathComponent("\(ddfPath)/\(facObj.name!) Thumb.jpg")
            try? thumbImageData?.write(to: thumbURL!)
        }
        
        //write hotspot thumbnail to folder
        if let hotspotThumbDict = facObj.hotspotsThumb
        {
            for hsName in hotspotThumbDict.keys
            {
                let parentPath = "\(ddfPath)/subFacility/\(hsName)"
                let subFacilityPath = documentsDirectory.appendingPathComponent(parentPath)
                if !FileManager.default.fileExists(atPath: subFacilityPath.path)
                {
                    try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(parentPath), withIntermediateDirectories: true)
                }
                let hsThumb = hotspotThumbDict[hsName]
                var hsTbImg:UIImage?
                let hsUrlTb = URL(string: hsThumb!)
                var hsThumbURL:URL?
                let hsTbData = try? Data(contentsOf: hsUrlTb!)
                hsTbImg = UIImage(data: hsTbData!)
                let hsThumbImgData = hsTbImg!.jpegData(compressionQuality: 1.0)
                hsThumbURL = documentsDirectory.appendingPathComponent("\(parentPath)/thumbnail.jpg")
                try? hsThumbImgData?.write(to: hsThumbURL!)
                //print(hsThumbURL!.path)
            }
        }
    }
    
    var testARtype = 0
    
    func addGadgetToDocumentDirectory(ddfPath:String, gadObj:GadgetClass){
        // get the documents directory url
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let originalURL = documentsDirectory.appendingPathComponent(ddfPath)
        try? FileManager.default.removeItem(at: originalURL)
        
        try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(ddfPath), withIntermediateDirectories: true)
        
        //Todo check Info.plist exists
        if (checkDocumentDirectoryAR(ddfPath: ddfPath))
        {
            let fileURL = documentsDirectory.appendingPathComponent(ddfPath).appendingPathComponent("Info.plist")
            
            do {
                // writes the image data to disk
                try FileManager.default.removeItem(at: fileURL)
                
            } catch {
                print("error removing file:", error)
            }
        }
        
        let dictWr:NSMutableDictionary = NSMutableDictionary.init()
        //saving values
        let gadNameObj = GADGET()
        dictWr.setObject(gadObj.id! , forKey: gadNameObj.id as NSCopying)
        dictWr.setObject(gadObj.name! , forKey: gadNameObj.name as NSCopying)
        dictWr.setObject(gadObj.level! , forKey: gadNameObj.level as NSCopying)
        dictWr.setObject(gadObj.entityType! , forKey: gadNameObj.entityType as NSCopying)
        dictWr.setObject(gadObj.desc! , forKey: gadNameObj.desc as NSCopying)
        dictWr.setObject(gadObj.timeStamp! , forKey: gadNameObj.timeStamp as NSCopying)
        dictWr.setObject(gadObj.arImageLink! , forKey: gadNameObj.arImageLink as NSCopying)
        dictWr.setObject(gadObj.baseImage! , forKey: gadNameObj.baseImage as NSCopying)
        dictWr.setObject(gadObj.arType! , forKey: gadNameObj.arType as NSCopying)
        dictWr.setObject(gadObj.baseImageDimHeight! , forKey: gadNameObj.baseImageDimHeight as NSCopying)
        dictWr.setObject(gadObj.baseImageDimWidth! , forKey: gadNameObj.baseImageDimWidth as NSCopying)
        dictWr.setObject(gadObj.arOffsetX! , forKey: gadNameObj.arOffsetX as NSCopying)
        dictWr.setObject(gadObj.arOffsetY! , forKey: gadNameObj.arOffsetY as NSCopying)
        dictWr.setObject(gadObj.arOffsetZ! , forKey: gadNameObj.arOffsetZ as NSCopying)
        
        //writing to Info.plist
        dictWr.write(toFile: documentsDirectory.appendingPathComponent(ddfPath + "/Info.plist").path, atomically: false)
        
        if (checkDocumentDirectoryAR(ddfPath: ddfPath))
        {
            print("YES")
        }
        
        //FOR TESTING PURPOSES
        //            testARtype += 1
        //            var testURL = ""
        //            var testBaseURL = ""
        //            if testARtype == 1
        //            {
        //                testURL = "https://www.godsavethepoints.com/wp-content/uploads/2018/08/One_farrer_hotel_singapore14.jpeg"
        //                testBaseURL = "https://www.godsavethepoints.com/wp-content/uploads/2018/08/One_farrer_hotel_singapore14.jpeg"
        //            } else if testARtype == 2
        //            {
        //                testURL = "https://docs.google.com/uc?id=17F91fcFNF6nqgcIbiX3MQ8BAvMsvdi85"
        //                testBaseURL = "https://docs.google.com/uc?id=12UpxILsbFu62ImKXF-H-8U1OsWzTncPC"
        //            }
        //FOR TESTING PURPOSES
        
        if gadObj.arImageLink! != ""
        {
            //write ar image to folder
            var image:UIImage?
            //Use this when the api is ready --> add the http://fpc, etc to the link
            //change link for bundle also
            
            //let urlImage = URL(string: testURL)
            let urlImage = URL(string: gadObj.arImageLink!)
            var imageURL:URL?
            if let ui = urlImage
            {
                let data = try? Data(contentsOf: ui)
                image = UIImage(data: data!)
                let imageData = image!.jpegData(compressionQuality: 1.0)
                imageURL = documentsDirectory.appendingPathComponent("\(ddfPath)/arImage.jpg")
                try? imageData?.write(to: imageURL!)
                print(imageURL!.path)
            }
        } else {
            let image = UIImage(named: "no-image")
            let imageData = image!.jpegData(compressionQuality: 1.0)
            var imageURL = documentsDirectory.appendingPathComponent("\(ddfPath)/arImage.jpg")
            try? imageData?.write(to: imageURL)
            print(imageURL.path)
        }
        
        if gadObj.baseImage! != ""
        {
            var baseImage:UIImage?
            //Use this when the api is ready --> add the http://fpc, etc to the link
            //change link for bundle also
            
            //let urlBaseImage = URL(string: testBaseURL)
            let urlBaseImage = URL(string: gadObj.baseImage!)
            var imageBaseURL:URL?
            if let ubi = urlBaseImage
            {
                let baseData = try? Data(contentsOf: ubi)
                baseImage = UIImage(data: baseData!)
                let imageBaseData = baseImage!.jpegData(compressionQuality: 1.0)
                imageBaseURL = documentsDirectory.appendingPathComponent("\(ddfPath)/baseImage.jpg")
                try? imageBaseData?.write(to: imageBaseURL!)
            }
        }
        
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func checkLocalDirectory(ddfPath:String) -> Bool{
        
        guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return false}
        guard let bundle = Bundle(url: bundleURL) else { return false}
        let infoURL = bundle.url(forResource: ddfPath+"/Info", withExtension: "plist")
        
        if (infoURL != nil){
            return true
        }
        
        return false
    }
    
    func checkDocumentDirectory(ddfPath:String) -> Bool{
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // choose a name for your image
        let fileName = "Info.plist"
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(ddfPath).appendingPathComponent(fileName)
        
        //check Info.plist//
        if (FileManager.default.fileExists(atPath: fileURL.path) ){
            return true
        }
        return false
    }
    
    func checkLocalDirectoryAR(ddfPath:String) -> Bool{
        
        guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return false}
        guard let bundle = Bundle(url: bundleURL) else { return false}
        let infoURL = bundle.url(forResource: ddfPath+"/Info", withExtension: "plist")
        
        if (infoURL != nil){
            return true
        }
        
        return false
    }
    
    func checkDocumentDirectoryAR(ddfPath:String) -> Bool{
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // choose a name for your image
        let fileName = "Info.plist"
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(ddfPath).appendingPathComponent(fileName)
        
        //check Info.plist//
        if (FileManager.default.fileExists(atPath: fileURL.path) ){
            return true
        }
        return false
    }
    
    //Read facility objects from json API
    func retrieveFacilties(completion: @escaping(_ result: String) -> Void) {
        // Set up the URL request
        //https://api.myjson.com/bins/1fvdcc
        //https://api.myjson.com/bins/nivy8 //yusof
        //https://thetfpc.com/.rest/delivery/facilities
        
        let facilitiesEndpoint: String = "https://api.myjson.com/bins/nivy8"
        //let facilitiesEndpoint: String = "https://thetfpc.com/.rest/delivery/facilities"
        guard let url = URL(string: facilitiesEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /facilties")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                //convert json data to dictionary
                guard let facilities = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                print("Retrieve Facilities!")
                //print(facilities["results"])
                let facArr = facilities["results"] as? [[String: AnyObject]]
                let facNum = facArr?.count as AnyObject
                print ("# of Facilities: \(facNum)")
                
                let dispatchGroup = DispatchGroup()
                
                //loop through facility results
                for facility in facArr!{
                    
                    print("---------------START---------------")
                    print(facility["@id"]!)
                    print(facility["name"]!)
                    print(facility["floorLevel"]!)
                    print(facility["entityType"]!)
                    print(facility["mgnl:lastModified"]!)
                    
                    var pFacility = "-1"
                    
                    if let pFac = facility["priority"] {
                        pFacility = pFac as! String
                    }
                    
                    print("Priority: \(pFacility)")
                    
                    var thumbNailURL = ""
                    if let vrThumbObj = facility["vrThumbnail"]
                    {
                        if let vrThumbLink = vrThumbObj["@link"]
                        {
                            thumbNailURL =  "https://thetfpc.com\(vrThumbLink as! String)"
                        } else {
                            thumbNailURL = ""
                        }
                    }
                    
                    var vrLink = ""
                    if let vrImageParent = facility["vrImage"]
                    {
                        if let vrImgLink = vrImageParent["@link"]
                        {
                            vrLink = vrImgLink as! String
                        } else {
                            //vrLink = vrImageParent as! String
                            vrLink = ""
                        }
                    }
                    
                    var hotspotsArr:[String] = []
                    var hotspotsName:[String] = []
                    var hotspotThumb:[String:String] = [:]
                    
                    var newTimeStamp: String?
                    
                    //checks for facility hotspots -->
                    if let hotsArr = facility["hotspots"] as? [String]
                    {
                        //original code
                        //hotspotsArr = test as! [String]
                        for hotspots in hotsArr
                        {
                            dispatchGroup.enter()
                            
                            //For testing purposes, use https://api.myjson.com/bins/1giwc0
                            //facility["hotspots"] stores hotspot json api url
                            let hotspotURL = URLRequest(url: URL(string: "https://thetfpc.com/.rest/delivery/subfacilities?@jcr:uuid=\(hotspots)")!)
                            
                            //let hotspotURL = URLRequest(url: URL(string: "https://api.myjson.com/bins/1bbk88")!)
                            
                            URLSession.shared.dataTask(with: hotspotURL) {
                                data, response, error in
                                if let data = data {
                                    do {
                                        
                                        guard let res = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                                            print("error trying to convert data to JSON")
                                            return
                                        }
                                        let hsLinkArr = res["results"] as? [[String: AnyObject]]
                                        for hs in hsLinkArr!
                                        {
                                            hotspotsName.append(hs["name"] as! String)
                                            
                                            let formatter = DateFormatter()
                                            formatter.locale = Locale(identifier: "en_US_POSIX")
                                            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                                            
                                            let facDate = formatter.date(from: facility["mgnl:lastModified"]! as! String)
                                            let hsDate = formatter.date(from: hs["mgnl:lastModified"]! as! String)
                                            
                                            var currNewTimeStamp:String?
                                            if Calendar.current.compare(facDate!, to: hsDate!, toGranularity: .minute) == .orderedAscending
                                            {
                                                currNewTimeStamp = hs["mgnl:lastModified"]! as! String
                                                print("HS HAS NEWER DATE ")
                                            } else {
                                                currNewTimeStamp = facility["mgnl:lastModified"]! as! String
                                                print("FACILITY HAS NEWER DATE")
                                            }
                                            
                                            if newTimeStamp != nil
                                            {
                                                let prevHsDate = formatter.date(from: newTimeStamp!)
                                                let currNewTSDate = formatter.date(from: currNewTimeStamp!)
                                                if Calendar.current.compare(prevHsDate!, to: currNewTSDate!, toGranularity: .second) == .orderedAscending
                                                {
                                                    newTimeStamp = currNewTimeStamp
                                                }
                                            } else {
                                                newTimeStamp = currNewTimeStamp
                                            }
                                            
                                            //get link of vr image for that particular hotspot
                                            if let vrHSimgObj = hs["vrImage"]
                                            {
                                                if let vrHSlink = vrHSimgObj["@link"]
                                                {
                                                    //uncomment to add hotspot image to hotspotsArr here
                                                    //hotspotsArr.append(vrHSlink as! String)
                                                    //hotspotsArr.append("https://docs.google.com/uc?id=1YUv7NrVpmiVXyDgLtBZOtOZOJNTW_BNH")
                                                    hotspotsArr.append("https://thetfpc.com\(vrHSlink as! String)")
                                                } else {
                                                    hotspotsArr.append("")
                                                }
                                            } else {
                                                hotspotsArr.append("")
                                            }
                                            //get link of vr thumbnail for that particular hotspot
                                            if let vrThumbnail = hs["vrThumbnail"]
                                            {
                                                if let vrTNlink = vrThumbnail["@link"]
                                                {
                                                    let fullLink = "https://thetfpc.com\(vrTNlink as! String)"
                                                    hotspotThumb["\(hs["name"]! as! String)"] = fullLink
                                                }
                                            }
                                        }
                                        
                                        //add here is url session is an async function, the assignToFacility function has to be executed after the url session task
                                        if hotspotsArr.count == hotsArr.count
                                        {
                                            print("FACILITY NAME: \(facility["name"]! as! String)")
                                            self.assignToFacility(id: facility["@id"]! as! String, name: facility["name"]! as! String, floorlevel: facility["floorLevel"]! as! String, entityType: facility["entityType"]! as! String, desc: facility["writeup"] as! String, timeStamp: newTimeStamp!, vrImageLink: vrLink, hotspotsArr: hotspotsArr, hotspotsName: hotspotsName, thumbNail: thumbNailURL, hotspotThumbs: hotspotThumb, priority: pFacility)
                                        }
                                        
                                        dispatchGroup.leave()
                                        
                                    } catch let error {
                                        print(error)
                                        
                                        dispatchGroup.leave()
                                    }
                                    
                                }
                            }.resume()
                        }
                        
                        //testing code
                        print("Hotspots test: \(hotsArr)")
                        
                        //hotspotsArr = ["https://docs.google.com/uc?id=1YUv7NrVpmiVXyDgLtBZOtOZOJNTW_BNH", "https://docs.google.com/uc?id=13TWA-OgvvUMH2GSaKW0uyz5uGqkMbBgC"]
                    }
                        //if hotspots doesn't exist for that particular facility
                    else
                    {
                        dispatchGroup.enter()
                        
                        self.assignToFacility(id: facility["@id"]! as! String, name: facility["name"]! as! String, floorlevel: facility["floorLevel"]! as! String, entityType: facility["entityType"]! as! String, desc: facility["writeup"]! as! String, timeStamp: facility["mgnl:lastModified"]! as! String, vrImageLink: vrLink, hotspotsArr: hotspotsArr, hotspotsName: hotspotsName, thumbNail: thumbNailURL, hotspotThumbs: hotspotThumb, priority: pFacility)
                        
                        dispatchGroup.leave()
                    }
                    
                    //                        if let category = facility["category"] as? String
                    //                        {
                    //                           print("Found Category!")
                    //                        }
                    //                        else
                    //                        {
                    //                            print("No Category Found!")
                    //                        }
                    print("----------------END----------------")
                }
                
                //URL session is an async function
                //Dispatch group to ensure code executes synchronously
                dispatchGroup.notify(queue: .main) {
                    print("Both functions complete 👍")
                    //call completion handler to end process when all codes in the functions are executed
                    completion("Success")
                }
                
                //completion("Success")
                
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        })
        
        task.resume()
    }
    
    func assignToFacility(id: String, name: String, floorlevel:String, entityType: String, desc: String, timeStamp: String, vrImageLink: String, hotspotsArr: [String], hotspotsName: [String], thumbNail: String, hotspotThumbs:[String:String],priority:String)
    {
        var facilityLink = ""
        if vrImageLink != ""
        {
            //                let retrievedLink = "https://drive.google.com/file/d/11YAtolzKoorCWln3CQwOKwF78SipOnAJ/view"
            //                let startIndex = retrievedLink.index(retrievedLink.startIndex, offsetBy: 32)
            //                let lastIndex = retrievedLink.index(retrievedLink.endIndex, offsetBy: -5)
            //                let facLinkId = retrievedLink[startIndex..<lastIndex]
            //                //test using facilityLink first
            //                facilityLink = "https://docs.google.com/uc?id=" + facLinkId
            facilityLink = "https://thetfpc.com\(vrImageLink)"
        }
        
        
        
        self.facilityObj = FacilityClass(id: id, name: name, floorlevel: floorlevel, entityType: entityType, desc: desc, timeStamp: timeStamp, vrImageLink: facilityLink, hotspotsArr: hotspotsArr, hotspotsName: hotspotsName, vrThumbNail: thumbNail, hotspotsThumb: hotspotThumbs,priority: priority)
        facilityArr.append(self.facilityObj!)
        
        //Check if Documents directory folder exists first
        let ddfPath = "VR/\(self.facilityObj!.entityType!)/\(self.facilityObj!.floorlevel!)/\(self.facilityObj!.name!)"
        let isDocumentDirectoryExist = self.checkDocumentDirectory(ddfPath: ddfPath)
        let isLocalDirectoryExist = self.checkLocalDirectory(ddfPath: ddfPath)
        print("isDocumentDirectoryExist: \(isDocumentDirectoryExist)")
        print("isLocalDirectoryExist: \(isLocalDirectoryExist)")
        
        //newly added Facility
        if !isDocumentDirectoryExist && !isLocalDirectoryExist
        {
            if let facObj = self.facilityObj
            {
                self.addFacilityToDocumentDirectory(ddfPath: ddfPath, facObj: facObj)
            }
        }
        //When the bundle(local) directory already contains the facility object but object does not exist in document directory
        if !isDocumentDirectoryExist && isLocalDirectoryExist
        {
            if let facObj = self.facilityObj
            {
                //2019-12-03T14:25:45.713+08:00
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let newDate = formatter.date(from: facObj.timeStamp!)
                print("New Date: \(facObj.timeStamp!)")
                var oldDate:Date?
                
                guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
                guard let bundle = Bundle(url: bundleURL) else { return }
                let infoURL = bundle.url(forResource: ddfPath+"/Info", withExtension: "plist")
                let xml = FileManager.default.contents(atPath: infoURL!.path)
                //get timestamp from infoplist
                //all facility objects contains an infoplist in their facility directory
                let timeStamp = try? PropertyListSerialization.propertyList(from: xml!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                let stringDate = timeStamp!["lastModified"]!
                print("Old Date: \(stringDate)")
                oldDate = formatter.date(from: stringDate as! String)
                //Compare the previous timestamp of that facility object to the newly retrieved timestamp
                if Calendar.current.compare(oldDate!, to: newDate!, toGranularity: .minute) != .orderedSame
                {
                    self.addFacilityToDocumentDirectory(ddfPath: ddfPath, facObj: facObj)
                    print("Different date")
                }
            }
        }
        //When facility object exists in both document and bundle directory or exists only in document directory
        if (isDocumentDirectoryExist && isLocalDirectoryExist) || (isDocumentDirectoryExist && !isLocalDirectoryExist)
        {
            if let facObj = self.facilityObj
            {
                //2019-12-03T14:25:45.713+08:00
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let newDate = formatter.date(from: facObj.timeStamp!)
                print("New Date: \(facObj.timeStamp!)")
                var oldDate:Date?
                
                try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(ddfPath), withIntermediateDirectories: true)
                // get the documents directory url
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let fileURL = documentsDirectory.appendingPathComponent("\(ddfPath)/Info.plist")
                let xml = FileManager.default.contents(atPath: fileURL.path)
                let timeStamp = try? PropertyListSerialization.propertyList(from: xml!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                let stringDate = timeStamp!["lastModified"]!
                print("Old Date: \(stringDate)")
                oldDate = formatter.date(from: stringDate as! String)
                //Compare infoplist timestamp to newly retrieved timestamp
                if Calendar.current.compare(oldDate!, to: newDate!, toGranularity: .minute ) != .orderedSame
                {
                    //If there's a change in date, add the facility to document directory
                    self.addFacilityToDocumentDirectory(ddfPath: ddfPath, facObj: facObj)
                    print("Different date")
                }
            }
        }
    }
    
    //Read gadget objects from json API
    func retrieveGadgets(completion: @escaping(_ result: String) -> Void)
    {
        //test api: https://api.myjson.com/bins/y8xxg
        //real api: https://thetfpc.com/.rest/delivery/gadgets
        
        //let facilitiesEndpoint: String = "https://thetfpc.com/.rest/delivery/gadgets"
        let facilitiesEndpoint: String = "https://api.myjson.com/bins/1c4xbs"
        guard let url = URL(string: facilitiesEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /facilties")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            
            do {
                //Convert json data to dictionary format
                guard let gadgets = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                print("Retrieve Gadgets!")
                //print(facilities["results"])
                let gadArr = gadgets["results"] as? [[String: AnyObject]]
                
                for gadget in gadArr!{
                    
                    print("---------------START---------------")
                    print(gadget["@id"]!)
                    print(gadget["name"]!)
                    print(gadget["level"]!)
                    print(gadget["entityType"]!)
                    print(gadget["mgnl:lastModified"]! as! String)
                    print(gadget["arImage"]!["@link"]!!)
                    
                    //Use then when there's an actual image
                    let arImgUrl = "https://thetfpc.com\(gadget["arImage"]!["@link"]!! as! String)"
                    let baseImgUrl = "https://thetfpc.com\(gadget["baseImage"]!["@link"]!! as! String)"
                    
                    var newBaseUrl = ""
                    if let checkBase = gadget["baseImage"]
                    {
                        if let baseUrl = checkBase["@link"]
                        {
                            newBaseUrl = "https://thetfpc.com\(baseUrl as! String)"
                        }
                    }
                    var newARurl = ""
                    if let checkAR = gadget["arImage"]
                    {
                        if let arUrl = checkAR["@link"]
                        {
                            newARurl = "https://thetfpc.com\(arUrl as! String)"
                        }
                    }
                    
                    self.gadgetObj = GadgetClass(id: gadget["@id"]! as! String, name: gadget["name"]! as! String, level: gadget["level"]! as! String, entityType: gadget["entityType"]! as! String, desc: gadget["writeup"]! as! String, timeStamp: gadget["mgnl:lastModified"]! as! String, arImageLink: newARurl, baseImage: newBaseUrl, arType: Int(gadget["arType"]! as! String)!, baseImageDimHeight: Double(gadget["baseImageDimensionHeight"]! as! String)!, baseImageDimWidth: Double(gadget["baseImageDimensionWidth"]! as! String)!, arOffsetY: Double(gadget["arOffsetY"]! as! String)!, arOffsetZ: Double(gadget["arOffsetZ"]! as! String)!, arOffsetX: Double(gadget["arOffsetX"]! as! String)!)
                    
                    gadgetArr.append(self.gadgetObj!)
                    
                    self.assignToGadget()
                    
                    print("----------------END----------------")
                }
                
                
                completion("Success")
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        })
        task.resume()
    }
    
    func assignToGadget()
    {
        //Check if Documents directory folder exists first
        var ddfPath = "AR/\(self.gadgetObj!.entityType!)/\(self.gadgetObj!.level!)/\(self.gadgetObj!.name!)"
        var isDocumentDirectoryExist = self.checkDocumentDirectoryAR(ddfPath: ddfPath)
        var isLocalDirectoryExist = self.checkLocalDirectoryAR(ddfPath: ddfPath)
        
        //newly added gadget --> write gadget object to document directory
        if !isDocumentDirectoryExist && !isLocalDirectoryExist
        {
            if let gadObj = self.gadgetObj
            {
                self.addGadgetToDocumentDirectory(ddfPath: ddfPath, gadObj: gadObj)
            }
        }
        //When the bundle(local) directory already contains the gadget object but object does not exist in document directory
        //Only write gadget object to document directory when there's an update --> difference in timestamp
        //**Read timestamp from bundle directory**
        if !isDocumentDirectoryExist && isLocalDirectoryExist
        {
            if let gadObj = self.gadgetObj
            {
                //2019-12-03T14:25:45.713+08:00
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let newDate = formatter.date(from: gadObj.timeStamp!)
                print("TESTING: \(newDate!)")
                var oldDate:Date?
                
                guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
                //get the bundle directory url
                guard let bundle = Bundle(url: bundleURL) else { return }
                let infoURL = bundle.url(forResource: ddfPath+"/Info", withExtension: "plist")
                let xml = FileManager.default.contents(atPath: infoURL!.path)
                //Read timestamp from bundle directory
                let timeStamp = try? PropertyListSerialization.propertyList(from: xml!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                var stringDate = timeStamp!["lastModified"]!
                print(stringDate)
                oldDate = formatter.date(from: stringDate as! String)
                //Compare gadget infoplist timestamp to newly retrieved timestamp
                if Calendar.current.compare(oldDate!, to: newDate!, toGranularity: .minute) != .orderedSame
                {
                    //If theres a change in timestamp, update the directory
                    self.addGadgetToDocumentDirectory(ddfPath: ddfPath, gadObj: gadObj)
                    print("Different date")
                }
            }
        }
        //gadget object exists in both document and bundle directory
        //Only write gadget object to document directory when there's an update --> difference in timestamp
        //**Read timestamp from document directory**
        if (isDocumentDirectoryExist && isLocalDirectoryExist) || (isDocumentDirectoryExist && !isLocalDirectoryExist)
        {
            if let gadObj = self.gadgetObj
            {
                //2019-12-03T14:25:45.713+08:00
                let formatter = DateFormatter()
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let newDate = formatter.date(from: gadObj.timeStamp!)
                print("TESTING: \(newDate!)")
                var oldDate:Date?
                
                // get the documents directory url
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let fileURL = documentsDirectory.appendingPathComponent("\(ddfPath)/Info.plist")
                let xml = FileManager.default.contents(atPath: fileURL.path)
                //Read timestamp from document directory
                let timeStamp = try? PropertyListSerialization.propertyList(from: xml!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                var stringDate = timeStamp!["lastModified"]!
                print(stringDate)
                oldDate = formatter.date(from: stringDate as! String)
                if Calendar.current.compare(oldDate!, to: newDate!, toGranularity: .minute) != .orderedSame
                {
                    self.addGadgetToDocumentDirectory(ddfPath: ddfPath, gadObj: gadObj)
                    print("Different date")
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLoc"
        {
            let locCon = segue.destination as! LocationViewController
        }
        else if segue.identifier == "showVR"
        {
            let vrCon = segue.destination as! VRViewController
        }
        else if segue.identifier == "showAboutUs"
        {
            let auCon = segue.destination as! AboutUsViewController
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showVRcontrol"
        {
            if chosenFacGlobal != nil && chosenLvlGlobal != nil && chosenBuildGlobal != nil
            {
                return true
            }
            else
            {
                VRerrLbl.text = "Please select location first!"
                showFlower = true
                return false
            }
        }
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//extension UINavigationController {
//
//    // Set the shouldAutorotate to False
//    override open var shouldAutorotate: Bool {
//       return false
//    }
//
//    // Specify the orientation.
//    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//       return .portrait
//    }
//}
