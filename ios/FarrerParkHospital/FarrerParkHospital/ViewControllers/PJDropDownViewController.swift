//
//  dropdownlist.swift
//  Aboutus
//
//  Created by FYPJ on 23/12/19.
//  Copyright © 2019 NYP. All rights reserved.
//

import UIKit

var categories:[String:[String:Any]] = [:]
var procedures:[String] = []

class PJDropDownViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblDropDown: UITableView!
    @IBOutlet weak var tblDropDownHC: NSLayoutConstraint!
    
    @IBOutlet weak var selectselectBtn: UIButton!
    @IBOutlet weak var versionLbl: UILabel!
    
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homebtn: bigCircleBtn!
    @IBAction func homeBtnPressed(_ sender: Any) {
        menu.animateBtn(homeBtn: homebtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    
  // var procedureitem : Procedure?
    
    var isTableVisible = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblDropDown.isHidden = true
        tblDropDown.delegate = self
        tblDropDown.dataSource = self
        //tblDropDownHC.constant = 0
        
       tblDropDown.estimatedRowHeight = 250 //Your estimated height
        tblDropDown.rowHeight = UITableView.automaticDimension
        
        selectselectBtn.titleEdgeInsets = UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblDropDown.isHidden = false
        versionLbl.isHidden = true
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        
        menu.currVC = self
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
        if procedures == []
        {
            isLoading()
            getPJlist(completion: {
                _ in
                DispatchQueue.main.async {
                    self.tblDropDown.reloadData()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.alert!.dismiss(animated: true, completion: nil)
                })
                print("Success")
            })
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }

    @IBAction func onClickbtn(_ sender: Any) {
//        if tblDropDown.isHidden
//        {
//            animate(toogle: true)
//        }
//        else
//        {
//            animate(toogle: false)
//        }
    }

    func animate(toogle: Bool)
    {
        if toogle {
            UIView.animate(withDuration: 0.3)
                   {
                       self.tblDropDown.isHidden = false
                    
                   }
        }
        else
        {
            UIView.animate(withDuration: 0.3)
            {
                 self.tblDropDown.isHidden = true
                
            }
        }
    }
    var alert:UIAlertController?
    var loadingIndicator:UIActivityIndicatorView?
    func isLoading(){
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator!.hidesWhenStopped = true
        loadingIndicator!.style = UIActivityIndicatorView.Style.gray
        loadingIndicator!.startAnimating();
        alert!.view.addSubview(loadingIndicator!)
        present(alert!, animated: true, completion: nil)
    }
    
    func getPJlist(completion: @escaping(_ result: String) -> Void)
    {

        //https://thetfpc.com/.rest/delivery/procedures
        //https://api.myjson.com/bins/1550ks
        //https://drive.google.com/uc?export=download&id=1rvg0_vtQ3mHdoRet-dYgzee1T19enkRT
        
        //let facilitiesEndpoint: String = "https://thetfpc.com/.rest/delivery/procedures"
        let facilitiesEndpoint: String = "https://drive.google.com/uc?export=download&id=1rvg0_vtQ3mHdoRet-dYgzee1T19enkRT"
        guard let url = URL(string: facilitiesEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /patientJourney")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let journey = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                print("Retrieved patient journey")
                
                let jourArr = journey["results"] as? [[String: AnyObject]]
                for jour in jourArr!
                {
                    if let jourName = jour["name"]
                    {
                        let jourNameStr = jourName as! String
                        categories[jourNameStr] = [:]
                        procedures.append(jourNameStr)
                        if let preAdmission = jour["preadmission"]
                        {
                            categories[jourNameStr]!["Pre-admission"] = preAdmission as! String
                        }else{
                            categories[jourNameStr]!["Pre-admission"] = "<p>PRE-ADMISSION</p>"
                        }
                        if let admission = jour["admission"]
                        {
                            categories[jourNameStr]!["Admission"] = admission as! String
                        }else{
                            categories[jourNameStr]!["Admission"] = "<p>ADMISSION</p>"
                        }
                        if let recovery = jour["recovery"]
                        {
                            categories[jourNameStr]!["Recovery"] = recovery as! String
                        }else{
                            categories[jourNameStr]!["Recovery"] = "<p>RECOVERY</p>"
                        }
                        if let discharge = jour["discharge"]
                        {
                            categories[jourNameStr]!["Discharge"] = discharge as! String
                        }else{
                            categories[jourNameStr]!["Discharge"] = "<p>DISCHARGE</p>"
                        }
                        if let afterCare = jour["aftercare"]
                        {
                            categories[jourNameStr]!["AfterCare"] = afterCare as! String
                        }else{
                            categories[jourNameStr]!["AfterCare"] = "<p>AFTERCARE</p>"
                        }
                        if let facilities = jour["facilities"]
                        {
                            //self.facLinks = []
                            if let facArr = facilities as? [String]
                            {
                                //categories[jourNameStr]!["Facility"] = facArr
                                for fac in facArr
                                {
                                    self.retrieveFacilities(facName: fac, prodName: jourNameStr)
                                }
                                
                            }
                        }else{
                            categories[jourNameStr]!["Facility"] = "<p>FACILITY</p>"
                        }
                    }
                }
                completion("Success")
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        })
        
        task.resume()
    }
    
    var facLinks:[String:[[String:String]]] = [:]
    var currentFacName = ""
    
    func retrieveFacilities(facName:String, prodName:String)
    {
        let facilitiesEndpoint: String = "https://thetfpc.com/.rest/delivery/facilities?@jcr:uuid=\(facName)"
        guard let url = URL(string: facilitiesEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /facilities")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let facilities = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                print("Retrieved facilities")
                
                let facArr = facilities["results"] as? [[String: AnyObject]]
                for fac in facArr!
                {
                    if self.facLinks[prodName] == nil
                    {
                        self.facLinks[prodName] = []
                    }
                    if let facName = fac["name"] as? String, let facLvl = fac["floorLevel"] as? String
                    {
                        self.facLinks[prodName]!.append(["name" : facName , "level" : facLvl])
                    }
                }
                categories[prodName]!["Facility"] = self.facLinks[prodName]
                //self.facLinks = []
            } catch {
                print("error trying to convert data to JSON")
                return
            }
        })
        task.resume()
    }
    
    // This function is triggered when a segue will be triggered.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "pushnext")
        {
            let dest = segue.destination as! PatientjourneyViewController
            let patientindex = tblDropDown.indexPathForSelectedRow?.row
            if patientindex != nil
            {
                let proceduremenu = procedures[patientindex!]
                dest.procedureName = proceduremenu
                prodNameGbl = proceduremenu
                print(categories)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return procedures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let title = procedures[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = title
        print(procedures)
        tblDropDown.showsVerticalScrollIndicator=true
        //Yusuf:This is for the flashing of the scrollbar
        tblDropDown.flashScrollIndicators()
        return cell!
    }

}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

