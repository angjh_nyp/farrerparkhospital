//
//  VRViewController.swift
//  projProto
//
//  Created by FYPJ on 3/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import UIKit
import GVRKit


//import PopupDialog

var chosenFacGlobal:String?
var chosenLvlGlobal:String?
var chosenBuildGlobal:String?
var previousLoc:Bool = false
var goBackPJ:Bool = false

//call delegates for gvr renderer and akpicker
class VRViewController: UIViewController, GVRWidgetViewDelegate, AKPickerViewDataSource, AKPickerViewDelegate, GVRRendererViewControllerDelegate {
    
    var panoView:GVRRendererView?
    var vrDisplayImg:UIImage?
    
    //renderer function is called to display vr image
    func renderer(for displayMode: GVRDisplayMode) -> GVRRenderer! {
        
        //initialise vr image as gvr image renderer
        let imageRenderer:GVRImageRenderer = GVRImageRenderer.init(image: vrDisplayImg!)
        imageRenderer.setSphericalMeshOfRadius(50, latitudes: 12, longitudes: 24, verticalFov: 180, horizontalFov: 360, meshType: .monoscopic)
        
        //add image renderer to scene renderlist
        //scene renderer is used to display the vr image
        let sceneRenderer:GVRSceneRenderer = GVRSceneRenderer.init()
        sceneRenderer.renderList.add(imageRenderer)
        
        if displayMode == GVRDisplayMode.embedded
        {
            sceneRenderer.hidesReticle = true
        }
        
        
        return sceneRenderer
    }
    
    var previousPJ:Bool = false

    @IBOutlet weak var imageVRView: UIView!
    @IBOutlet weak var vrbutton: bigCircleBtn!
    @IBOutlet weak var arbutton: bigCircleBtn!
    @IBOutlet weak var backBtn: bigCircleBtn!
    @IBAction func backBtnPressed(_ sender: Any) {
        let prevCon = self.navigationController?.popViewController(animated: true)
        previousLoc = true
        nextVR = true
    }
    
    @IBOutlet weak var versionLbl: UILabel!
    
    @IBOutlet weak var tutorialImg: UIImageView!
    let tapScreen = UITapGestureRecognizer()
    
    @IBOutlet weak var imageAKPicker: AKPickerView!
    @IBOutlet weak var imageLabel: UILabel!
    
    var menu:flowerMenu?
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBAction func homeBtnPressed(_ sender: Any) {
        if !previousPJ
        {
            menu!.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: backBtn)
        } else {
            self.navigationController!.popViewController(animated: true)
            goBackPJ = true
        }
    }
    @IBOutlet weak var homeBtn: bigCircleBtn!
    
    //var photoArray:[String] = ["bakery.jpg", "mainLobby.jpg"]
    var photoArray:[UIImage] = []
    var photoSelect : [UIImage] = []
    var photoThumb: [String:UIImage] = [:]
    var photoName : [String] = []
    var shortPhotoName:[String] = []
    
    var currentView: UIView?
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    
    
    var origin = ""
    var it = 0
    var location = ""
    var message = ""
    var isPaused = true
    var image: UIImage!
    var chosenFac:String?
    var chosenLvl:String?
    var chosenBuilding:String?
    
    //Delegate of AKpicker
    //AKpicker display facility hotspots and main vr image in a horizontal format
    //AKpicker only displays when hotspots exist for that particular facility
    func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
        if photoSelect.count == 1
        {
            return 0
        }
        return photoSelect.count
    }
    
    func pickerView(_ pickerView: AKPickerView, imageForItem item: Int) -> UIImage {
        if photoThumb[photoName[item]] != nil
        {
            return photoThumb[photoName[item]]!.imageWithSize(CGSize(width: 120, height: 120))
        }
        return photoSelect[item].imageWithSize(CGSize(width: 120, height: 120))
    }
    
    func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
        
        vrDisplayImg = photoSelect[item]
        
        panoView!.removeFromSuperview()
        
         let vc = GVRRendererViewController()
         vc.delegate = self
         panoView = vc.rendererView
        //Add leo
        self.addChild(vc)
        
         let pvTrail = NSLayoutConstraint(item: panoView!, attribute: .trailing, relatedBy: .equal, toItem: imageVRView, attribute: .trailing, multiplier: 1.0, constant: 0)
         let pvLead = NSLayoutConstraint(item: panoView!, attribute: .leading, relatedBy: .equal, toItem: imageVRView, attribute: .leading, multiplier: 1.0, constant: 0)
         let pvTop = NSLayoutConstraint(item: panoView!, attribute: .top, relatedBy: .equal, toItem: imageVRView, attribute: .top, multiplier: 1.0, constant: 0)
         let pvBott = NSLayoutConstraint(item: panoView!, attribute: .bottom, relatedBy: .equal, toItem: imageVRView, attribute: .bottom, multiplier: 1.0, constant: 0)
         imageVRView.addSubview(panoView!)
         imageVRView.bringSubviewToFront(imageLabel)
         imageVRView.bringSubviewToFront(imageAKPicker)
         imageVRView.bringSubviewToFront(vrbutton)
         imageVRView.bringSubviewToFront(arbutton)
         panoView!.translatesAutoresizingMaskIntoConstraints = false
         view.addConstraints([pvTrail, pvLead, pvTop, pvBott])
        
        imageLabel.text = "\nEXPERIENCE REALITY \n\n \(photoName[item])"
        it = item
        location = photoName[0]
        image = photoSelect[item]
        message  = "This is one of the hotspot in " + origin
    }
    
    
    
    func widgetView(_ widgetView: GVRWidgetView!, didLoadContent content: Any!) {
    }

    func widgetView(_ widgetView: GVRWidgetView!, didFailToLoadContent content: Any!,
      withErrorMessage errorMessage: String!)  {
    }

    func widgetView(_ widgetView: GVRWidgetView!, didChange displayMode: GVRWidgetDisplayMode) {
        currentView = widgetView
        currentDisplayMode = displayMode
        if currentView == imageVRView && currentDisplayMode != GVRWidgetDisplayMode.embedded {
          view.isHidden = true
        } else {
          view.isHidden = false
        }
    }
    
    func widgetViewDidTap(_ widgetView: GVRWidgetView!) {
        // 1
        guard currentDisplayMode != GVRWidgetDisplayMode.embedded else {return}
        // 2
        if currentView == imageVRView {
          photoArray.append(photoArray.removeFirst())
          //imageVRView?.load(photoArray.first!, of: GVRPanoramaImageType.mono)
        }
    }
    
    var backNewCon:[NSLayoutConstraint] = []
    var backOrgCon:[NSLayoutConstraint] = []
    var homeOrgTrailCon:NSLayoutConstraint?
    @IBOutlet weak var backTrailCon: NSLayoutConstraint!
    @IBOutlet weak var backBtmCon: NSLayoutConstraint!
    @IBOutlet weak var homeBtmCon: NSLayoutConstraint!
    @IBOutlet weak var homeTrailCon: NSLayoutConstraint!
    
    
    //need add constraint changes when first launch this page
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu!.orientationChange(view: size)
        if size.width > size.height
        {
            view.removeConstraints([backTrailCon, backBtmCon])
            let leadBack = NSLayoutConstraint(item: backBtn, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -145)
            let trailBtm = NSLayoutConstraint(item: backBtn, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -30)
            backNewCon = [leadBack, trailBtm]
            view.addConstraints([leadBack, trailBtm])
        } else {
            view.removeConstraints(backNewCon)
            view.addConstraints([backTrailCon, backBtmCon])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        versionLbl.isHidden = true
        
        backOrgCon = [backTrailCon, backBtmCon]
        homeOrgTrailCon = homeTrailCon
        
        if view.frame.size.width > view.frame.size.height
        {
            view.removeConstraints([backTrailCon, backBtmCon])
            let leadBack = NSLayoutConstraint(item: backBtn, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -145)
            let trailBtm = NSLayoutConstraint(item: backBtn, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: -30)
            backNewCon = [leadBack, trailBtm]
            view.addConstraints([leadBack, trailBtm])
        }
        
        if chosenBuilding != nil && chosenLvl != nil && chosenFac != nil
        {
            chosenBuildGlobal = chosenBuilding
            chosenLvlGlobal = chosenLvl
            chosenFacGlobal = chosenFac
        }
        
        photoSelect = []
        
        if previousPJ
        {
            homeBtn.setImage(UIImage(named: "btn_return912"), for: .normal)
            homeBtn.setBackgroundImage(UIImage(named: "btn_return912"), for: .normal)
            
            backBtn.isHidden = true
            vrbutton.isHidden = true
            arbutton.isHidden = true
        } else {
            menu = flowerMenu()
            menu!.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        }
        
        imageAKPicker.delegate = self
        imageAKPicker.dataSource = self
        
        photoName.append(chosenFacGlobal!)
        shortPhotoName.append(chosenFacGlobal!)
        imageLabel.text = "\nEXPERIENCE REALITY \n\n \(photoName[0])"
        
        getVRinfo()
        
        if photoSelect.count <= 1
        {
            imageAKPicker.backgroundColor = nil
        }
        
        vrDisplayImg = photoSelect[0]
        
        let vc = GVRRendererViewController.init()
        vc.delegate = self
        panoView = vc.rendererView
        self.addChild(vc)
        let pvTrail = NSLayoutConstraint(item: panoView!, attribute: .trailing, relatedBy: .equal, toItem: imageVRView, attribute: .trailing, multiplier: 1.0, constant: 0)
        let pvLead = NSLayoutConstraint(item: panoView!, attribute: .leading, relatedBy: .equal, toItem: imageVRView, attribute: .leading, multiplier: 1.0, constant: 0)
        let pvTop = NSLayoutConstraint(item: panoView!, attribute: .top, relatedBy: .equal, toItem: imageVRView, attribute: .top, multiplier: 1.0, constant: 0)
        let pvBott = NSLayoutConstraint(item: panoView!, attribute: .bottom, relatedBy: .equal, toItem: imageVRView, attribute: .bottom, multiplier: 1.0, constant: 0)
        imageVRView.addSubview(panoView!)
        imageVRView.bringSubviewToFront(imageLabel)
        imageVRView.bringSubviewToFront(imageAKPicker)
        imageVRView.bringSubviewToFront(vrbutton)
        imageVRView.bringSubviewToFront(arbutton)
        panoView!.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([pvTrail, pvLead, pvTop, pvBott])
    
        //panoView!.frame = CGRect(x: 0, y: 0, width: imageVRView.frame.width, height: imageVRView.frame.height)
//        getVRImage()
//        getVRthumbNails()
        
//        imageVRView.load(photoArray.first!, of: GVRPanoramaImageType.mono)
//        imageVRView.enableCardboardButton = true
//        imageVRView.enableFullscreenButton = true
        
//        getHotspotImages()
//        getHotspotThumbNails()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if launchBefore
        {
            tutorialImg.isHidden = true
        } else {
            launchBefore = true
        }
        
        tutorialImg.isUserInteractionEnabled = true
        
        tapScreen.addTarget(self, action: #selector(tapSceenAction))
        tutorialImg.addGestureRecognizer(tapScreen)
    }
    
    @objc func tapSceenAction()
    {
        tutorialImg.isHidden = true
    }
    
    var vrImageDir:[String] = []
    
    func getVRinfo()
    {
        if chosenBuildGlobal != nil && chosenLvlGlobal != nil && chosenFacGlobal != nil
        {
            let ddfPath = "VR/\(chosenBuildGlobal!)/\(chosenLvlGlobal!)/\(chosenFacGlobal!)"
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let facDir = documentsDirectory?.appendingPathComponent("\(ddfPath)")
            if let facDirURL = facDir
            {
                if FileManager.default.fileExists(atPath: facDirURL.path)
                {
                    //get VR image from doc directory
                    let imageDir = facDirURL.appendingPathComponent("\(chosenFacGlobal!).jpg")
                    if FileManager.default.fileExists(atPath: imageDir.path)
                    {
                        let uiImage = UIImage.init(contentsOfFile: imageDir.path)
                        if let uiImg = uiImage
                        {
                            //photoArray.append(uiImg)
                            photoSelect.append(uiImg)
                        }
                    }
                    
                    //get VR thumbnail from doc directory
                    let vrThumbDirect = facDirURL.appendingPathComponent("\(chosenFacGlobal!) Thumb.jpg")
                    if FileManager.default.fileExists(atPath: vrThumbDirect.path)
                    {
                        let uiImage = UIImage.init(contentsOfFile: vrThumbDirect.path)
                        photoThumb[chosenFacGlobal!] = uiImage!
                    }
                    
                    //get hotspot from doc directory
                    let imageHSDir = facDirURL.appendingPathComponent("subFacility")
                    if FileManager.default.fileExists(atPath: imageHSDir.path)
                    {
                        let fileURLs = try? FileManager.default.contentsOfDirectory(at: imageHSDir, includingPropertiesForKeys: nil, options: [])
                        
                        if let hsFolders = fileURLs
                        {
                            for hsFolder in hsFolders
                            {
                                let hotspotName = hsFolder.lastPathComponent
                                
                                //get hotspot images from doc directory
                                let imageHSJPG = hsFolder.appendingPathComponent("hotspot.jpg").path
                                if FileManager.default.fileExists(atPath: imageHSJPG)
                                {
                                    let uiImage = UIImage.init(contentsOfFile: imageHSJPG)
                                    
                                    photoSelect.append(uiImage!)
                                    //photoArray.append(uiImage!)
                                    photoName.append("\(chosenFacGlobal!) - \(hotspotName)")
                                    shortPhotoName.append("\(hotspotName)")
                                }
                                //get hotspot thumbnails from doc directory
                                let imgThumb = hsFolder.appendingPathComponent("thumbnail.jpg").path
                                if FileManager.default.fileExists(atPath: imgThumb)
                                {
                                    let uiImage = UIImage.init(contentsOfFile: imgThumb)
                                    photoThumb["\(chosenFacGlobal!) - \(hotspotName)"] = uiImage!
                                }
                            }
                        }
                    }
                }
                
                else {
                    guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
                    guard let bundle = Bundle(url: bundleURL) else { return }
                    let infoURL = bundle.url(forResource: ddfPath, withExtension: nil)
                    if let infoUrl = infoURL
                    {
                        if FileManager.default.fileExists(atPath: infoUrl.path)
                        {
                            //get VR image from local directory
                            let imageDir = infoUrl.appendingPathComponent("\(chosenFacGlobal!).jpg")
                            if FileManager.default.fileExists(atPath: imageDir.path)
                            {
                                let uiImage = UIImage.init(contentsOfFile: imageDir.path)
                                if let uiImg = uiImage
                                {
                                    //photoArray.append(uiImg)
                                    photoSelect.append(uiImg)
                                }
                            }
                            
                            //get VR thumbnail from local directory
                            let vrThumbDirect = infoUrl.appendingPathComponent("\(chosenFacGlobal!) Thumb.jpg")
                            if FileManager.default.fileExists(atPath: vrThumbDirect.path)
                            {
                                let uiImage = UIImage.init(contentsOfFile: vrThumbDirect.path)
                                photoThumb[chosenFacGlobal!] = uiImage!
                            }
                            
                            //get hotspot from local directory
                            let imageHSDir = infoUrl.appendingPathComponent("subFacility")
                            if FileManager.default.fileExists(atPath: imageHSDir.path)
                            {
                                let fileURLs = try? FileManager.default.contentsOfDirectory(at: imageHSDir, includingPropertiesForKeys: nil, options: [])
                                
                                if let hsFolders = fileURLs
                                {
                                    for hsFolder in hsFolders
                                    {
                                        //get hotspot images from local directory
                                        let hotspotName = hsFolder.lastPathComponent
                                        let imageHSJPG = hsFolder.appendingPathComponent("hotspot.jpg").path
                                        if FileManager.default.fileExists(atPath: imageHSJPG)
                                        {
                                            let uiImage = UIImage.init(contentsOfFile: imageHSJPG)
                                            //let crop = uiImage?.cropToRect(rect: CGRect(x: 1700, y: 900, width: 1730, height: 1100))
                                            photoSelect.append(uiImage!)
                                            //photoArray.append(uiImage!)
                                            photoName.append("\(chosenFacGlobal!) - \(hotspotName)")
                                            shortPhotoName.append("\(hotspotName)")
                                        }
                                        //get hotspot thumbnails from local directory
                                        let imgThumb = hsFolder.appendingPathComponent("thumbnail.jpg").path
                                        if FileManager.default.fileExists(atPath: imgThumb)
                                        {
                                            let uiImage = UIImage.init(contentsOfFile: imgThumb)
                                            hsThumbDir.append(hotspotName)
                                            photoThumb["\(chosenFacGlobal!) - \(hotspotName)"] = uiImage!
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
//    func getVRImage()
//    {
//        if chosenBuildGlobal != nil && chosenLvlGlobal != nil && chosenFacGlobal != nil
//        {
//            let ddfPath = "VR/\(chosenBuildGlobal!)/\(chosenLvlGlobal!)/\(chosenFacGlobal!)/\(chosenFacGlobal!).jpg"
//            //try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(ddfPath), withIntermediateDirectories: true)
//
//            // get the documents directory url
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//            let imageDir = documentsDirectory?.appendingPathComponent("\(ddfPath)")
//            if FileManager.default.fileExists(atPath: imageDir!.path)
//            {
//                let uiImage = UIImage.init(contentsOfFile: imageDir!.path)
//                vrImageDir.append("\(ddfPath)")
//                photoArray.append(uiImage!)
//                photoSelect.append(uiImage!)
//            }
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: ddfPath, withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                if FileManager.default.fileExists(atPath: infoUrl.path)
//                {
//                    let imageURL = infoURL?.path
//                    if let imageUrl = imageURL
//                    {
//                        let uiImage = UIImage.init(contentsOfFile: imageUrl)
//                        if !vrImageDir.contains("\(ddfPath)")
//                        {
//                            photoArray.append(uiImage!)
//                            photoSelect.append(uiImage!)
//                            vrImageDir.append("\(ddfPath)")
//                        }
//                    }
//                }
//            }
//        }
//    }
    
//    var vrThumbDir:[String] = []
//    //the key of photoThumb is chosenFacGlobal --> facility name
//    //if implementing thumbnails for hotspots then, (chosenFacGlobal + hotspotName) is the key
//    func getVRthumbNails()
//    {
//        if chosenBuildGlobal != nil && chosenLvlGlobal != nil && chosenFacGlobal != nil
//        {
//            let ddfPath = "VR/\(chosenBuildGlobal!)/\(chosenLvlGlobal!)/\(chosenFacGlobal!)/\(chosenFacGlobal!) Thumb.jpg"
//            //try? FileManager.default.createDirectory(at: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(ddfPath), withIntermediateDirectories: true)
//
//            // get the documents directory url
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//            let imageDir = documentsDirectory?.appendingPathComponent("\(ddfPath)")
//            if FileManager.default.fileExists(atPath: imageDir!.path)
//            {
//                let uiImage = UIImage.init(contentsOfFile: imageDir!.path)
//                vrThumbDir.append("\(ddfPath)")
//                photoThumb[chosenFacGlobal!] = uiImage!
//
//            }
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: ddfPath, withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                if FileManager.default.fileExists(atPath: infoUrl.path)
//                {
//                    let imageURL = infoURL?.path
//                    if let imageUrl = imageURL
//                    {
//                        let uiImage = UIImage.init(contentsOfFile: imageUrl)
//                        if !vrThumbDir.contains("\(ddfPath)")
//                        {
//                            photoThumb[chosenFacGlobal!] = uiImage!
//                            vrThumbDir.append("\(ddfPath)")
//                        }
//                    }
//                }
//            }
//        }
//    }
//
    var hsThumbDir:[String] = []
//
//    func getHotspotThumbNails()
//    {
//        if chosenBuildGlobal != nil && chosenLvlGlobal != nil && chosenFacGlobal != nil
//        {
//            let ddfPath = "VR/\(chosenBuildGlobal!)/\(chosenLvlGlobal!)/\(chosenFacGlobal!)/subFacility"
//
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//            let subFacilityDocs = documentsDirectory?.appendingPathComponent("\(ddfPath)")
//
//            if FileManager.default.fileExists(atPath: subFacilityDocs!.path)
//            {
//                let fileURLs = try? FileManager.default.contentsOfDirectory(at: subFacilityDocs!, includingPropertiesForKeys: nil, options: [])
//
//                if let hsFolders = fileURLs
//                {
//                    for hsFolder in hsFolders
//                    {
//                        let hotspotName = hsFolder.lastPathComponent
//                        let imgThumb = hsFolder.appendingPathComponent("thumbnail.jpg").path
//                        if FileManager.default.fileExists(atPath: imgThumb)
//                        {
//                            let uiImage = UIImage.init(contentsOfFile: imgThumb)
//                            hsThumbDir.append(hotspotName)
//                            photoThumb["\(chosenFacGlobal!) - \(hotspotName)"] = uiImage!
//                        }
//                    }
//                }
//            }
//
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: "\(ddfPath)/subFacility", withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                if FileManager.default.fileExists(atPath: infoUrl.path)
//                {
//                    let fileURLs = try? FileManager.default.contentsOfDirectory(at: infoUrl, includingPropertiesForKeys: nil, options: [])
//
//                    if let hsFolders = fileURLs
//                    {
//                        for hsFolder in hsFolders
//                        {
//                            let hotspotName = hsFolder.lastPathComponent
//                            let imgThumb = hsFolder.appendingPathComponent("thumbnail.jpg").path
//                            if FileManager.default.fileExists(atPath: imgThumb)
//                            {
//                                if !hsThumbDir.contains(hotspotName)
//                                {
//                                    let uiImage = UIImage.init(contentsOfFile: imgThumb)
//                                    hsThumbDir.append(hotspotName)
//                                    photoThumb["\(chosenFacGlobal!) - \(hotspotName)"] = uiImage!
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    func getHotspotImages()
//    {
//        //let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//        if chosenBuildGlobal != nil && chosenLvlGlobal != nil && chosenFacGlobal != nil
//        {
//            let ddfPath = "VR/\(chosenBuildGlobal!)/\(chosenLvlGlobal!)/\(chosenFacGlobal!)"
//
//            // get the documents directory url
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//            let imageHSDir = documentsDirectory?.appendingPathComponent("\(ddfPath)/subFacility")
//            if FileManager.default.fileExists(atPath: imageHSDir!.path)
//            {
//                let fileURLs = try? FileManager.default.contentsOfDirectory(at: imageHSDir!, includingPropertiesForKeys: nil, options: [])
//
//                if let hsFolders = fileURLs
//                {
//                    for hsFolder in hsFolders
//                    {
//                        let hotspotName = hsFolder.lastPathComponent
//                        let imageHSJPG = hsFolder.appendingPathComponent("hotspot.jpg").path
//                        if FileManager.default.fileExists(atPath: imageHSJPG)
//                        {
//                            let uiImage = UIImage.init(contentsOfFile: imageHSJPG)
//                            //let crop = uiImage?.cropToRect(rect: CGRect(x: 1700, y: 900, width: 1730, height: 1100))
//                            photoSelect.append(uiImage!)
//                            photoArray.append(uiImage!)
//                            photoName.append("\(chosenFacGlobal!.uppercased()) - \(hotspotName.uppercased())")
//                        }
//                    }
//                }
//            }
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: "\(ddfPath)/subFacility", withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                if FileManager.default.fileExists(atPath: infoUrl.path)
//                {
//                    let fileURLs = try? FileManager.default.contentsOfDirectory(at: infoUrl, includingPropertiesForKeys: nil, options: [])
//
//                    if let hsFolders = fileURLs
//                    {
//                        for hsFolder in hsFolders
//                        {
//                            let hotspotName = hsFolder.lastPathComponent
//                            let imageHSJPG = hsFolder.appendingPathComponent("hotspot.jpg").path
//                            if FileManager.default.fileExists(atPath: imageHSJPG)
//                            {
//                                let uiImage = UIImage.init(contentsOfFile: imageHSJPG)
//                                //let crop = uiImage?.cropToRect(rect: CGRect(x: 1700, y: 900, width: 1730, height: 1100))
//                                if !photoName.contains("\(chosenFacGlobal!.uppercased()) - \(hotspotName.uppercased())")
//                                {
//                                    photoSelect.append(uiImage!)
//                                    photoArray.append(uiImage!)
//                                    photoName.append("\(chosenFacGlobal!.uppercased()) - \(hotspotName.uppercased())")
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        print(photoSelect)
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showHome"
        {
            let dest = segue.destination as! coverPageViewController
            showFlower = true
        }

        if segue.identifier == "showAR"
        {
            let dest = segue.destination as! ARViewController
        }
        
        if segue.identifier == "backToLoc"
        {
            let dest = segue.destination as! LocationViewController
        }
    }
    
}

extension UIImage {
    
    func cropToRect(rect: CGRect!) -> UIImage? {
        
        let scaledRect = CGRect(x: rect.origin.x * self.scale, y: rect.origin.y * self.scale, width: rect.size.width * self.scale, height: rect.size.height * self.scale);
        
        
        guard let imageRef: CGImage = self.cgImage?.cropping(to:scaledRect)
            else {
                return nil
        }
        
        let croppedImage: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        return croppedImage
    }
}

extension VRViewController {
    @objc func canRotate() -> Void {}
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
        if (self.isMovingFromParent) {
          UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    
    }
}

extension GVRRendererViewController {
    @objc func canRotate() -> Void {}
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
        if (self.isMovingFromParent) {
          UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }
    
    }
}

