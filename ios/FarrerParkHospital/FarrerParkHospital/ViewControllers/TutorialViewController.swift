//
//  TutorialViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 21/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet weak var tutorialImg: UIImageView!
    let tapScreen = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tutorialImg.isUserInteractionEnabled = true
        
        tapScreen.addTarget(self, action: #selector(tapSceenAction))
        tutorialImg.addGestureRecognizer(tapScreen)
    }
    
    @objc func tapSceenAction()
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "coverCtrl") as! coverPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
