//
//  PersonalDataViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 21/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit

class PersonalDataViewController: UIViewController {

    @IBOutlet weak var contentLbl: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
//    @IBOutlet weak var contentLbl: UILabel!
    @IBAction func returnBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        contentLbl.isEditable = false
       contentLbl.dataDetectorTypes = UIDataDetectorTypes.all;
        
        let descLbl = NSString(format: PDPP().htmlStyle as NSString, "\(PDPP().description)") as String
        let htmlWriteUp = try? NSMutableAttributedString(data: Data(descLbl.utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        contentLbl.attributedText = htmlWriteUp
        titleLbl.text = PDPP().title
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //Yusuf:This is for the flashing of the scrollbar
        contentLbl.flashScrollIndicators()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
