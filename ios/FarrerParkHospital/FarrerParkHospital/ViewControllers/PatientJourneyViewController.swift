//
//  PatientjourneyViewController.swift
//  Aboutus
//
//  Created by FYPJ on 11/12/19.
//  Copyright © 2019 NYP. All rights reserved.
//

import UIKit
import QuartzCore

var prodNameGbl:String?
var PJStepsClicked:[String] = []
var currPJStepsClicked:[String:[String]] = [:]

class PatientjourneyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet var popupView: UIView!
    @IBOutlet var facTablePop: UIView!
    @IBOutlet weak var facTableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    
    @IBOutlet weak var PreAdmission: UIButton!
    @IBOutlet weak var Admission: UIButton!
    @IBOutlet weak var Facility: UIButton!
    @IBOutlet weak var Recovery: UIButton!
    @IBOutlet weak var Discharge: UIButton!
    @IBOutlet weak var AfterCare: UIButton!
    
    @IBOutlet weak var versionLbl: UILabel!
    
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBOutlet weak var returnBtn: bigCircleBtn!
    
    @IBAction func returnBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func homeBtnPressed(_ sender: Any) {
        menu.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: returnBtn)
    }
    
    @IBAction func PApressed(_ sender: UIButton) {
        callPopUp(prodStep: "Pre-admission", sender: button, prodStepBtn: sender)
    }
    @IBAction func AdmissionPressed(_ sender: UIButton) {
        callPopUp(prodStep: "Admission", sender: button1, prodStepBtn: sender)
    }
    @IBAction func FacilityPressed(_ sender: UIButton) {
        if button2.backgroundColor == UIColor.black {
            button2.backgroundColor = UIColor.white
        }
        if button2.layer.borderColor == UIColor.lightGray.cgColor {
            button2.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
        }
        
        facTablePop.center = view.center
        facTablePop.alpha = 1
        facTablePop.transform = CGAffineTransform(scaleX: 1.2, y: 1.6)

        self.view.addSubview(facTablePop)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [],  animations: {
            //use if you want to darken the background
            //self.viewDim.alpha = 0.8
            //go back to original form
            self.facTablePop.transform = .identity
        })
        self.crossButtonPosition()
        
        Facility.setTitleColor(UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0), for: .normal)
        button2.layer.borderWidth = 15
        changeDesc(keyStr: "Facility")
        if (!currPJStepsClicked[prodNameGbl!]!.contains("Facility"))
        {
            //PJStepsClicked.append("Facility")
            currPJStepsClicked[prodNameGbl!]!.append("Facility")
        }
    }
    @IBAction func RecoveryPressed(_ sender: UIButton) {
        callPopUp(prodStep: "Recovery", sender: button3, prodStepBtn: sender)
    }
    @IBAction func DischargePressed(_ sender: UIButton) {
        callPopUp(prodStep: "Discharge", sender: button4, prodStepBtn: sender)
    }
    @IBAction func AfterCarePressed(_ sender: UIButton) {
        callPopUp(prodStep: "AfterCare", sender: button5, prodStepBtn: sender)
    }
    
    @IBOutlet weak var closeButton: bigCircleBtn!
    
    @IBAction func closebtn(_ sender: Any)
    {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
        //use if you wish to darken the background
          //self.viewDim.alpha = 0
          self.popupView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)

        }) { (success) in
          self.popupView.removeFromSuperview()
        }

    }
    
    @IBOutlet weak var context: UILabel!
    
    var procedureName : String?
    
    @IBOutlet weak var UIview: UIView!
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currPJStepsClicked[prodNameGbl!] == nil
        {
            currPJStepsClicked[prodNameGbl!] = []
        }
        
        // first button
        button.layer.cornerRadius = 20
        button.layer.borderWidth = 10
        button.layer.borderColor = UIColor.lightGray.cgColor
        
        //second button
        button1.layer.cornerRadius = 20
        button1.layer.borderWidth = 10
        button1.layer.borderColor = UIColor.lightGray.cgColor
        
        //third button
        button2.layer.cornerRadius = 20
        button2.layer.borderWidth = 10
        button2.layer.borderColor = UIColor.lightGray.cgColor
        
        
        button3.layer.cornerRadius = 20
        button3.layer.borderWidth = 10
        button3.layer.borderColor = UIColor.lightGray.cgColor
        
        button4.layer.cornerRadius = 20
        button4.layer.borderWidth = 10
        button4.layer.borderColor = UIColor.lightGray.cgColor
        
        button5.layer.cornerRadius = 20
        button5.layer.borderWidth = 10
        button5.layer.borderColor = UIColor.lightGray.cgColor
        
        if let prodName = prodNameGbl
        {
            titleLabel.text = prodName
        }
    }
    
    var crossBtn = UIButton()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        
        versionLbl.isHidden = true
        menu.currVC = self
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
        
        crossBtn.layer.cornerRadius = 20
        crossBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        crossBtn.setImage(UIImage(named: "btn_close"), for: .normal)
        crossBtn.setBackgroundImage(UIImage(named: "btn_close"), for: .normal)
        crossBtn.isHidden = true
        crossBtn.addTarget(self, action: #selector(crossBtnPressed), for: .touchUpInside)
        
        if goBackPJ
        {
            crossButtonPosition()
            goBackPJ = false
        }
    }
    
    @objc func crossBtnPressed()
    {
        self.crossBtn.isHidden = true
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
        //use if you wish to darken the background
          //self.viewDim.alpha = 0
            if(self.view.subviews.contains(self.popupView))
            {
                self.popupView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                self.popupView.removeFromSuperview()
            } else if(self.view.subviews.contains(self.facTablePop))
            {
                self.facTablePop.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                self.facTablePop.removeFromSuperview()
            }
        }) { (success) in
            //self.popupView.removeFromSuperview()
        }
    }
    
    func crossButtonPosition()
    {
        if(view.subviews.contains(popupView))
        {
            let cornerX = popupView.frame.width
            let cornerCoord = CGPoint(x: cornerX-15, y: -15)
            let newPoint = popupView.convert(cornerCoord, to: view)
            crossBtn.frame = CGRect(x: newPoint.x, y: newPoint.y, width: 40, height: 40)
        } else if(view.subviews.contains(facTablePop))
        {
            let cornerX = facTablePop.frame.width
            let cornerCoord = CGPoint(x: cornerX-15, y: -15)
            let newPoint = facTablePop.convert(cornerCoord, to: view)
            crossBtn.frame = CGRect(x: newPoint.x, y: newPoint.y, width: 40, height: 40)
        }
        view.addSubview(crossBtn)
        crossBtn.isHidden = false
    }
    
    var lineArray:[CAShapeLayer] = []

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        line.lineWidth = 8.0
        linePath.lineWidth = 20.0
        linePath.move(to: CGPoint(x: button.center.x, y: button.frame.maxY))
        linePath.addLine(to: CGPoint(x: button.center.x, y: button1.frame.minY))
        line.path = linePath.cgPath
        line.strokeColor = UIColor.black.cgColor
        self.view.layer.insertSublayer(line, below: blurredScreen.layer)
        lineArray.append(line)
        
        let line2 = CAShapeLayer()
        let linePath2 = UIBezierPath()
        line2.lineWidth = 8.0
        linePath2.lineWidth = 20.0
        linePath2.move(to: CGPoint(x: button1.center.x, y: button1.frame.maxY))
        linePath2.addLine(to: CGPoint(x: button1.center.x, y: button2.frame.minY))
        line2.path = linePath2.cgPath
        line2.strokeColor = UIColor.black.cgColor
        self.view.layer.insertSublayer(line2, below: blurredScreen.layer)
        lineArray.append(line2)
        
        let line3 = CAShapeLayer()
        let linePath3 = UIBezierPath()
        line3.lineWidth = 8.0
        linePath3.lineWidth = 20.0
        linePath3.move(to: CGPoint(x: button2.center.x, y: button2.frame.maxY))
        linePath3.addLine(to: CGPoint(x: button2.center.x, y: button3.frame.minY))
        line3.path = linePath3.cgPath
        line3.strokeColor = UIColor.black.cgColor
        self.view.layer.insertSublayer(line3, below: blurredScreen.layer)
        lineArray.append(line3)
        
        let line4 = CAShapeLayer()
        let linePath4 = UIBezierPath()
        line4.lineWidth = 8.0
        linePath4.lineWidth = 20.0
        linePath4.move(to: CGPoint(x: button3.center.x, y: button3.frame.maxY))
        linePath4.addLine(to: CGPoint(x: button3.center.x, y: button4.frame.minY))
        line4.path = linePath4.cgPath
        line4.strokeColor = UIColor.black.cgColor
        self.view.layer.insertSublayer(line4, below: blurredScreen.layer)
        lineArray.append(line4)
        
        let line5 = CAShapeLayer()
        let linePath5 = UIBezierPath()
        line5.lineWidth = 8.0
        linePath5.lineWidth = 20.0
        linePath5.move(to: CGPoint(x: button4.center.x, y: button4.frame.maxY))
        linePath5.addLine(to: CGPoint(x: button4.center.x, y: button5.frame.minY))
        line5.path = linePath5.cgPath
        line5.strokeColor = UIColor.black.cgColor
        self.view.layer.insertSublayer(line5, below: blurredScreen.layer)
        lineArray.append(line5)
        
        if let currClick = currPJStepsClicked[prodNameGbl!]
        {
            for pj in currClick
            {
                if (pj == "Pre-admission")
                {
                    restoreBtnState(pj: button, pjLblBtn: PreAdmission)
                } else if (pj == "Admission")
                {
                    restoreBtnState(pj: button1, pjLblBtn: Admission)
                } else if (pj == "Facility")
                {
                    restoreBtnState(pj: button2, pjLblBtn: Facility)
                } else if (pj == "Recovery")
                {
                    restoreBtnState(pj: button3, pjLblBtn: Recovery)
                } else if (pj == "Discharge")
                {
                    restoreBtnState(pj: button4, pjLblBtn: Discharge)
                } else
                {
                    restoreBtnState(pj: button5, pjLblBtn: AfterCare)
                }
            }
        }
    }
    
    func restoreBtnState(pj:UIButton, pjLblBtn:UIButton)
    {
        pj.backgroundColor = UIColor.white
        pj.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
        pj.layer.borderWidth = 15
        
        pjLblBtn.setTitleColor(UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0), for: .normal)
    }
    
    var facArr:[[String:String]] = [[:]]
    var facNameArr:[String] = []
    
    func changeDesc(keyStr:String)
    {
        if keyStr != "Facility"
        {
            let descLbl = NSString(format: "<span style=\'font-family: Titillium-Semibold; font-size: 15\'>%@</span>" as NSString, "\(categories[prodNameGbl!]![keyStr]!)") as! String

            let descStr = try? NSMutableAttributedString(data: Data(descLbl.utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            guard let finalDesc = descStr else { return }
            context.attributedText = finalDesc
        } else {
            facArr = categories[prodNameGbl!]![keyStr] as! [[String:String]]
            for fac in facArr
            {
                facNameArr.append(fac["name"]!)
                facTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let title = facNameArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = title
        return cell!
    }
    
    func callPopUp(prodStep:String, sender:UIButton, prodStepBtn:UIButton)
    {
        if sender.backgroundColor == UIColor.black {
            sender.backgroundColor = UIColor.white
        }
        popupView.center = view.center
        popupView.alpha = 1
        popupView.transform = CGAffineTransform(scaleX: 1.2, y: 1.6)

        self.view.addSubview(popupView)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [],  animations: {
            //use if you want to darken the background
            //self.viewDim.alpha = 0.8
            //go back to original form
            self.popupView.transform = .identity
        })
        self.crossButtonPosition()
        if sender.layer.borderColor == UIColor.lightGray.cgColor {
            sender.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
        }
        
        prodStepBtn.setTitleColor(UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0), for: .normal)
        sender.layer.borderWidth = 15
        changeDesc(keyStr: prodStep)
        
        if (!currPJStepsClicked[prodNameGbl!]!.contains(prodStep))
        {
            //PJStepsClicked.append(prodStep)
            currPJStepsClicked[prodNameGbl!]!.append(prodStep)
        }
    }
    
    //button pre-admission
    @IBAction func buttontapped1(_ sender: UIButton) {
        callPopUp(prodStep: "Pre-admission", sender: sender, prodStepBtn: PreAdmission)
    }
    
    @IBAction func hide(_ sender: Any) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
        //use if you wish to darken the background
          //self.viewDim.alpha = 0
          self.popupView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)

        }) { (success) in
          self.popupView.removeFromSuperview()

        }

    }
    
    //button admission
    @IBAction func button2tapped(_ sender: UIButton) {
        callPopUp(prodStep: "Admission", sender: sender, prodStepBtn: Admission)
    }
    
    //button facility
    @IBAction func buttontapped3(_ sender: UIButton) {
        if sender.backgroundColor == UIColor.black {
            sender.backgroundColor = UIColor.white
        }
        if sender.layer.borderColor == UIColor.lightGray.cgColor {
            sender.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
        }
        
        facTablePop.center = view.center
        facTablePop.alpha = 1
        facTablePop.transform = CGAffineTransform(scaleX: 1.2, y: 1.6)

        self.view.addSubview(facTablePop)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [],  animations: {
            //use if you want to darken the background
            //self.viewDim.alpha = 0.8
            //go back to original form
            self.facTablePop.transform = .identity
        })
        self.crossButtonPosition()
        
        Facility.titleLabel?.textColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
        button2.layer.borderWidth = 15
        changeDesc(keyStr: "Facility")
        if (!currPJStepsClicked[prodNameGbl!]!.contains("Facility"))
        {
            //PJStepsClicked.append("Facility")
            currPJStepsClicked[prodNameGbl!]!.append("Facility")
        }
    }
    
    //button Recovery
    @IBAction func buttontapped4(_ sender: UIButton) {
        callPopUp(prodStep: "Recovery", sender: sender, prodStepBtn: Recovery)
    }

    //button discharge
    @IBAction func buttontapped5(_ sender: UIButton) {
        callPopUp(prodStep: "Discharge", sender: sender, prodStepBtn: Discharge)
    }
    
    //button After Care
    @IBAction func buttontapped6(_ sender: UIButton) {
        callPopUp(prodStep: "AfterCare", sender: sender, prodStepBtn: AfterCare)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVRfromPJ"
        {
            let stepIndex = facTableView.indexPathForSelectedRow?.row
            if stepIndex != nil
            {
                let facName = facArr[stepIndex!]["name"]
                let facLvl = facArr[stepIndex!]["level"]
                for fac in facilityArr
                {
                    if fac.name == facName && fac.floorlevel == facLvl
                    {
                        chosenFacGlobal = fac.name
                        chosenLvlGlobal = fac.floorlevel
                        chosenBuildGlobal = fac.entityType
                    }
                }
            }
            let dest = segue.destination as! VRViewController
            dest.previousPJ = true
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

