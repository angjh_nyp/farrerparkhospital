//
//  ContactUsViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 21/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBAction func homeBtnPressed(_ sender: Any) {
        menu.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    @IBOutlet weak var versionLbl: UILabel!
    
    var desc:[NSMutableAttributedString] = []
    var img:[UIImage] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return desc.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension //125
    }
    
//

//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 40
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1
        {
            let number = URL(string: "tel://63631818")
            UIApplication.shared.open(number!, options: [:], completionHandler: nil)
        } else if indexPath.row == 3
        {
            let email = "enquiries@farrerpark.com"
            if let url = URL(string: "mailto:\(email)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else if indexPath.row == 0
        {
            let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "FarrerMapCtrl") as! FPHLocationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cuCell", for: indexPath) as! ContactUsCell
        let currImg = img[indexPath.row]
        let currDesc = desc[indexPath.row]
        cell.contactImg.image = currImg
        cell.contactDesc.attributedText = currDesc
        cell.contactDesc.sizeToFit()
        cell.frame = cell.contactDesc.frame
        if indexPath.row == 2
        {
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        versionLbl.isHidden = true
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        
        menu.currVC = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 180
        tableView.tableFooterView = UIView(frame: .zero)
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pin = UIImage(named: "contactUsPin")
        let phone = UIImage(named: "contactUsPhone")
        let fax = UIImage(named: "contactUsFax")
        let mail = UIImage(named: "contactUsMail")
        
        img = [pin!, phone!, fax!, mail!]
        
        let pinTitleAttr = [ NSMutableAttributedString.Key.font: UIFont(name: "Titillium-Semibold", size: 25), NSAttributedString.Key.foregroundColor : UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)]
        let normalAttr = [ NSMutableAttributedString.Key.font: UIFont(name: "Titillium-Regular", size: 20), NSAttributedString.Key.foregroundColor : UIColor.gray]
        let normalRedAttr = [ NSMutableAttributedString.Key.font: UIFont(name: "Titillium-Regular", size: 20), NSAttributedString.Key.foregroundColor : UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)]
        
        let pinTitle = NSAttributedString(string: "Farrer Park Hospital", attributes: pinTitleAttr)
        let pinDesc = NSAttributedString(string: "\n1 Farrer Park Station Road,\n#02-01 Connexion,\nSingapore 217562", attributes: normalRedAttr)
        let pinFullDesc = NSMutableAttributedString()
        pinFullDesc.append(pinTitle)
        pinFullDesc.append(pinDesc)
        
        let phoneNum = NSMutableAttributedString(string: "(65) 6363 1818", attributes: normalRedAttr)
        let faxNum = NSMutableAttributedString(string: "(65) 6705 2728", attributes: normalAttr)
        
        let mailDesc = NSMutableAttributedString(string: "enquiries@farrerpark.com", attributes: normalRedAttr)
        
        desc = [pinFullDesc, phoneNum, faxNum, mailDesc]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTnC"
        {
            let dest = segue.destination as! TermsConditionViewController
        } else if segue.identifier == "showPD"
        {
            let desy = segue.destination as! PersonalDataViewController
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


