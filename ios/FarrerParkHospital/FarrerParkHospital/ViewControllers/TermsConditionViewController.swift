//
//  TermsConditionViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 3/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit

class TermsConditionViewController: UIViewController {
      
//    @IBOutlet weak var lblcontent: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var lblcontent: UITextView!
    
    @IBAction func returnBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //Yusuf:This is for the flashing of the scrollbar
        lblcontent.flashScrollIndicators()
    }
    
      override func viewDidLoad() {
          super.viewDidLoad()
        
        lblcontent.isEditable = false
        lblcontent.dataDetectorTypes = UIDataDetectorTypes.all
        
        let descLbl = NSString(format: TNC().htmlStyle as NSString, "\(TNC().description)") as String
        let htmlWriteUp = try? NSMutableAttributedString(data: Data(descLbl.utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        lblcontent.attributedText = htmlWriteUp
        titleLabel.text = TNC().title
       
      }
}



