//
//  LocationViewController.swift
//  projProto
//
//  Created by FYPJ on 28/11/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import UIKit

var buildingLevelChosen:Int = 0
var buildingChosen:String = "HOS"

class LocationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //Please Enter Levels in Order
    var hospitalLevels:[Int] = []
    var hotelLevels:[Int] = []
    var facilities:[[String:String]] = []
    var tableViewDataSource:[String] = []
    var levelsArr:[Int] = []
    
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBAction func homeBtnPressed(_ sender: UIButton) {
        menu.animateBtn(homeBtn: sender, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    @IBOutlet weak var versionLbl: UILabel!
    
    var sectionLvlArr:[Int:Int] = [:]
    
    //Set up table for facility display for a specific level
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let noOfRows = facilities2[buildingChosen]![section].value.count
        return noOfRows
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return facilities2[buildingChosen]!.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = facilities2[buildingChosen]![section].key
        return "Level \(title)"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //       let view = UIView()
    //        view.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
    //        return view
    //    }
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let view = UIView(frame: CGRect(x:0,y:0,width: tableView.frame.width,height: 40))
    //        view.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
    //
    //        let lbl = UILabel(frame: CGRect(x:15,y:0,width: view.frame.width - 15,height: 40))
    //        let title = facilities2[buildingChosen]![section].key
    //        lbl.text =  "Level \(title)"
    //        lbl.textColor = .white
    //        lbl.numberOfLines = 1
    //        lbl.font = UIFont(name: "Titillium-Semibold", size: 17)
    //        view.addSubview(lbl)
    //
    //        return view
    //    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "facilityCell", for: indexPath)
        let cellSect = indexPath.section
        let facilityInfo = facilities2[buildingChosen]![cellSect].value[indexPath.row]
        cell.textLabel?.text = facilityInfo
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //state is changed as scrollview just started scrolling and user had already tapped
        if scrollView.panGestureRecognizer.state == .changed
        {
            let section = tableView.indexPathsForVisibleRows![0].section
            let level = facilities2[buildingChosen]![section].key
            if buildingLevelChosen != level
            {
                deselectAllLevelButtons()
                buildingLevelChosen = level
                for button in buildingLevelsButtonArray
                {
                    if button.titleLabel?.text == "\(level)"
                    {
                        button.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
                        button.layer.borderColor = UIColor.white.cgColor
                        button.setTitleColor(.white, for: .normal)
                    }
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //state is possible as scrollview stopped scrolling and user had already tapped
        if scrollView.panGestureRecognizer.state == .possible
        {
            let section = tableView.indexPathsForVisibleRows![0].section
            let level = facilities2[buildingChosen]![section].key
            if buildingLevelChosen != level
            {
                deselectAllLevelButtons()
                buildingLevelChosen = level
                for button in buildingLevelsButtonArray
                {
                    if button.titleLabel?.text == "\(level)"
                    {
                        button.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
                        button.layer.borderColor = UIColor.white.cgColor
                        button.setTitleColor(.white, for: .normal)
                    }
                }
            }
        }
    }
    
    
    //Function trigger when there's a change in phone orientation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    var levelCollectionViewIndex:Int = -1
    var buildingLevelsButtonArray:[UIButton] = []
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var selectLocLabel: UILabel!
    
    //Set up the collection view of levels
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "btnCell", for: indexPath)
        let levelButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = UIColor.white
            button.layer.cornerRadius = 45/2
            button.clipsToBounds = true
            for i in 0 ..< collectionSource.count {
                //print(i)
                if indexPath.row == i {
                    button.setTitle(String(collectionSource[i]), for: .normal)
                }
            }
            button.titleLabel?.font = UIFont(name: "Titillium-Semibold", size: 17)
            button.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
            button.layer.borderWidth = 3
            button.setTitleColor(.black, for: .normal)
            button.frame.size = CGSize(width: 45, height: 45)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(selectLevel), for: .touchUpInside)
            return button
        }()
        //When collection view is reloaded --> level is selected
        //check if the current indexPath is same as the index of the level selected
        //change color of button if so
        if levelCollectionViewIndex == indexPath.row {
            levelButton.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
            levelButton.layer.borderColor = UIColor.white.cgColor
            levelButton.setTitleColor(.white, for: .normal)
        }
        else {
            levelButton.backgroundColor = .white
            levelButton.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
            levelButton.setTitleColor(.black, for: .normal)
        }
        cell.contentView.addSubview(levelButton)
        buildingLevelsButtonArray.append(levelButton)
        if buildingLevelsButtonArray.count == 1
        {
            levelButton.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
            levelButton.layer.borderColor = UIColor.white.cgColor
            levelButton.setTitleColor(.white, for: .normal)
        }
        if switchEntity
        {
            //only the first level button of that entity clicked should be red
            levelButton.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
            levelButton.layer.borderColor = UIColor.white.cgColor
            levelButton.setTitleColor(.white, for: .normal)
            switchEntity = false
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 70, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Get the index of selected level
        levelCollectionViewIndex = indexPath.row
        //Reload collectionView
        collectionView.reloadData()
    }
    
    
    @IBAction func facilityPressed(_ sender: Any) {
        if tableView.isHidden {
            animate(toggle: true)
        } else {
            animate(toggle: false)
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var facilityBtn: UIButton!
    
    var collectionSource:[Int] = []
    
    
    @IBAction func connexionSeg(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            collectionSource = hospitalLevels
            buildingChosen = "HOS"
            tableView.isHidden = true
            facilityBtn.isHidden = true
            collectionView.reloadData()
        case 1:
            collectionSource = hotelLevels
            buildingChosen = "HOT"
            tableView.isHidden = true
            facilityBtn.isHidden = true
            collectionView.reloadData()
        default:
            break
        }
    }
    
    var switchEntity = false
    
    @IBOutlet weak var hosBtn: UIButton!
    @IBOutlet weak var hotBtn: UIButton!
    @IBAction func hosButtonPressed(_ sender: Any) {
        
        switchEntity = true
        
        DispatchQueue.main.async {
            //self.hosBtn.isHighlighted = true
            self.hosBtn.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
            self.hosBtn.layer.borderWidth = 2.0
            //self.hotBtn.isHighlighted = false
            
            //remove the border
            self.hotBtn.layer.borderWidth = 0.0
        }
        collectionSource = hospitalLevels
        buildingChosen = "HOS"
        collectionView.reloadData()
        tableView.reloadData()
    }
    @IBAction func hotButtonPressed(_ sender: Any) {
        
        switchEntity = true
        
        DispatchQueue.main.async {
            //self.hotBtn.isHighlighted = true
            //self.hosBtn.isHighlighted = false
            self.hotBtn.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
            self.hotBtn.layer.borderWidth = 2.0
            
            //remove the border
            self.hosBtn.layer.borderWidth = 0.0
        }
        
        collectionSource = hotelLevels
        buildingChosen = "HOT"
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    var BuildingPlusLevel:String = ""
    
    @objc func selectLevel(sender: UIButton)
    {
        deselectAllLevelButtons()
        
        buildingLevelChosen = Int(sender.currentTitle!)!
        sender.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
        sender.layer.borderColor = UIColor.white.cgColor
        sender.setTitleColor(.white, for: .normal)
        
        //Get all locations for the current level that is selected
        BuildingPlusLevel = buildingChosen + String(buildingLevelChosen)
        for values in facilities {
            if BuildingPlusLevel == values["Level"] {
                //Set the locations to the data source of the tableview
                tableViewDataSource.append("\(values["Location"]!) : \(values["priority"]!)")
            }
        }
        var section = 0
        for value in facilities2["HOS"]!
        {
            if value.key == buildingLevelChosen
            {
                section = try! facilities2["HOS"]!.firstIndex(where: {result in result == value})!
            }
        }
        let indexPath = IndexPath(row: 0, section: section)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        
        print(BuildingPlusLevel)
        print(sender.frame.size)
    }
    
    func deselectAllLevelButtons(){
        for levels in buildingLevelsButtonArray {
            let button = levels
            button.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
            button.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
            button.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
            button.setTitleColor(UIColor.black, for: .normal)
        }
    }
    
    func changeColor(myBtn: UIButton, btnNum:[UIButton])
    {
        let color = myBtn.layer.borderColor
        myBtn.layer.borderColor = myBtn.backgroundColor?.cgColor
        myBtn.backgroundColor = UIColor(cgColor: color!)
        myBtn.layer.masksToBounds = false
        myBtn.layer.shadowColor = UIColor.gray.cgColor
        myBtn.layer.shadowOpacity = 0.2
        myBtn.layer.shadowOffset = .zero
        myBtn.layer.shadowRadius = 1
        if !myBtn.backgroundColor!.isEqual(UIColor.white)
        {
            for x in 0..<btnNum.count
            {
                btnNum[x].layer.borderColor = myBtn.backgroundColor?.cgColor
                btnNum[x].backgroundColor = UIColor(cgColor: myBtn.layer.borderColor!)
            }
        }
    }
    
    func animate(toggle: Bool)
    {
        if toggle {
            self.tableView.isHidden = false
        } else {
            self.tableView.isHidden = true
        }
    }
    
    @IBOutlet weak var locSegment: UISegmentedControl!
    override func viewWillAppear(_ animated: Bool) {
        
        //        hosBtn.adjustsImageWhenHighlighted = false
        //        hotBtn.adjustsImageWhenHighlighted = true
        
        menu.currVC = self
        //Get version of project and set that as the value of the versionLbl
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        versionLbl.isHidden = true
        
        //submitBtn.layer.cornerRadius = 15
        if !nextVR
        {
            //tableView.isHidden = true
            //facilityBtn.isHidden = true
            getHotInfo()
            getHosInfo()
            levelsArr = hospitalLevels
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
        
        //        if locSegment.selectedSegmentIndex == 0
        //        {
        //            buildingChosen = "HOS"
        //        } else {
        //            buildingChosen = "HOT"
        //        }
        
        //        getHoslevels()
        //        getHotlevels()
        //        getFacilityByHosLevel(lvlArr: hospitalLevels)
        //        getFacilityByHotLevel(lvlArr: hotelLevels)
        
        collectionSource = hospitalLevels
        
        hosBtn.layer.shadowColor = UIColor.gray.cgColor
        hosBtn.layer.shadowOpacity = 3
        
        hotBtn.layer.shadowColor = UIColor.gray.cgColor
        hotBtn.layer.shadowOpacity = 3
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //default hos selected
        //self.hosBtn.isHighlighted = true
        //self.hotBtn.isHighlighted = false
        self.hosBtn.layer.borderColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0).cgColor
        self.hosBtn.layer.borderWidth = 2.0
        
        // Do any additional setup after loading the view.
    }
    
    var facilities2:[String:[(key: Int, value: [String])]] = [:]
    var facilities22:[String:[(key: Int, value: [FacilityClass])]] = [:]
    var tmpFacArr:[FacilityClass] = []
    var pfacilities2:[String:[(key: Int, value: [String])]] = [:]
    var plevelFac:[Int:[FacilityClass]] = [:]
    
    //Read hospital levels and locations from global variable, facilityArr
    //Append to facility array in the format, [["Level": "HOS1", "Location":"Red Apple"]]
    //Before sorting the data in ascending order
    func getHosInfo()
    {
        if facilities2["HOS"] == nil
        {
            facilities2["HOS"] = []
        }
        
        if pfacilities2["HOS"] == nil
        {
            pfacilities2["HOS"] = []
        }
        
        plevelFac = [:]
        
        for fac in facilityArr
        {
            if let entityType = fac.entityType
            {
                if entityType == "FPH"
                {
                    if let fl = Int(fac.floorlevel!)
                    {
                        if !hospitalLevels.contains(fl)
                        {
                            hospitalLevels.append(fl)
                        }
                        
                        facilities.append(["Level":"HOS\(fl)", "Location":fac.name! , "priority":fac.priority!])
                        
                        if plevelFac[fl] == nil
                        {
                            plevelFac[fl] = []
                        }
                        
                        //To Remove ! HardCode to test priority
                        //                        if (fac.name == "The Farm@Farrer")
                        //                        {
                        //                            fac.priority = "2"
                        //                        }
                        
                        plevelFac[fl]!.append(fac)
                    }
                }
            }
        }
        
        facilities22["HOS"] = plevelFac.sorted(by: { $0.key < $1.key })
        var tmpString:[String] = []
        
        for facLvl in facilities22["HOS"]!
        {
            //print("level: \(facLvl.key)")
            tmpString = []
            
            tmpFacArr = facLvl.value.sorted { (FacilityClass1, FacilityClass2) -> Bool in
                Int(FacilityClass1.priority!)! < Int(FacilityClass2.priority!)!
            }
            
            for facObj in tmpFacArr
            {
                //print(facObj.priority)
                //print(facObj.name)
                tmpString.append(facObj.name!)
            }
            
            pfacilities2["HOS"]?.append((Int(facLvl.key),tmpString))
            
        }
        
        //facilities2["HOS"] = levelFac.sorted(by: { $0.key < $1.key })
        
        facilities2["HOS"] = pfacilities2["HOS"]
        
        let finalLevel = hospitalLevels.sorted()
        hospitalLevels = finalLevel
    }
    
    //Read hotel levels and locations from global variable, facilityArr
    func getHotInfo()
    {
        if facilities2["HOT"] == nil
        {
            facilities2["HOT"] = []
        }
        
        if pfacilities2["HOT"] == nil
        {
            pfacilities2["HOT"] = []
        }
        
        for fac in facilityArr
        {
            if let entityType = fac.entityType
            {
                if entityType == "OFH"
                {
                    if let fl = Int(fac.floorlevel!)
                    {
                        if !hotelLevels.contains(fl)
                        {
                            hotelLevels.append(fl)
                        }
                        facilities.append(["Level":"HOT\(fl)", "Location":fac.name!])
                        
                        
                        if plevelFac[fl] == nil
                        {
                            plevelFac[fl] = []
                        }
                        
                        plevelFac[fl]!.append(fac)
                    }
                }
            }
        }
        
        facilities22["HOT"] = plevelFac.sorted(by: { $0.key < $1.key })
        var tmpString:[String] = []
        
        for facLvl in facilities22["HOT"]!
        {
            //print("level: \(facLvl.key)")
            tmpString = []
            
            tmpFacArr = facLvl.value.sorted { (FacilityClass1, FacilityClass2) -> Bool in
                Int(FacilityClass1.priority!)! < Int(FacilityClass2.priority!)!
            }
            
            for facObj in tmpFacArr
            {
                //print(facObj.priority)
                //print(facObj.name)
                tmpString.append(facObj.name!)
            }
            
            pfacilities2["HOT"]?.append((Int(facLvl.key),tmpString))
            
        }
        
        facilities2["HOT"] = pfacilities2["HOT"]
        
        //facilities2["HOT"] = levelFac.sorted(by: { $0.key < $1.key })
        let finalLevel = hotelLevels.sorted()
        hotelLevels = finalLevel
    }
    
//    func getHoslevels()
//    {
//        //get the documents directory url
//        let vrFPHpath = "VR/FPH"
//        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(vrFPHpath)
//        if FileManager.default.fileExists(atPath: documentsDirectory.path)
//        {
//            let directoryContents = try? FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//            let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//            let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//            var subdirNamesInt:[Int] = []
//            for name in subdirNamesStr
//            {
//                if let intName = Int(name)
//                {
//                    hospitalLevels.append(intName)
//                }
//            }
//        }
//        guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//        guard let bundle = Bundle(url: bundleURL) else { return }
//        let infoURL = bundle.url(forResource: vrFPHpath, withExtension: nil)
//        if let infoUrl = infoURL
//        {
//            if FileManager.default.fileExists(atPath: infoURL!.path)
//            {
//                let directoryContents = try? FileManager.default.contentsOfDirectory(at: infoURL!, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//                let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//                let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//                var subdirNamesInt:[Int] = []
//                for name in subdirNamesStr
//                {
//                    if let intName = Int(name)
//                    {
//                        if !hospitalLevels.contains(intName)
//                        {
//                            hospitalLevels.append(intName)
//                        }
//                    }
//                }
//            }
//        }
//        let finalLevel = hospitalLevels.sorted()
//        hospitalLevels = finalLevel
//    }
//
//    func getHotlevels()
//    {
//        //get the documents directory url
//        let vrOFHpath = "VR/OFH"
//        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(vrOFHpath)
//        if FileManager.default.fileExists(atPath: documentsDirectory.path)
//        {
//            let directoryContents = try? FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//            let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//            let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//            var subdirNamesInt:[Int] = []
//            for name in subdirNamesStr
//            {
//                if let intName = Int(name)
//                {
//                    hotelLevels.append(intName)
//                }
//            }
//        }
//        guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//        guard let bundle = Bundle(url: bundleURL) else { return }
//        let infoURL = bundle.url(forResource: vrOFHpath, withExtension: nil)
//        if let infoUrl = infoURL
//        {
//            if FileManager.default.fileExists(atPath: infoUrl.path)
//            {
//                let directoryContents = try? FileManager.default.contentsOfDirectory(at: infoURL!, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//                let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//                let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//                var subdirNamesInt:[Int] = []
//                for name in subdirNamesStr
//                {
//                    if let intName = Int(name)
//                    {
//                        if !hotelLevels.contains(intName)
//                        {
//                            hotelLevels.append(intName)
//                        }
//                    }
//                }
//            }
//        }
//        let finalLevel = hotelLevels.sorted()
//        hotelLevels = finalLevel
//    }
    
//    func getFacilityByHosLevel(lvlArr:[Int])
//    {
//        for level in lvlArr
//        {
//            //get the documents directory url
//            let vrFPHpath = "VR/FPH/\(level)"
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(vrFPHpath)
//            if FileManager.default.fileExists(atPath: documentsDirectory.path)
//            {
//                let directoryContents = try? FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//                let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//                let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//                for name in subdirNamesStr
//                {
//                    facilities.append(["Level":"HOS\(level)", "Location":name])
//                }
//            }
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: vrFPHpath, withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                if FileManager.default.fileExists(atPath: infoUrl.path)
//                {
//                    let directoryContents = try? FileManager.default.contentsOfDirectory(at: infoURL!, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//                    let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//                    let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//                    for name in subdirNamesStr
//                    {
//                        if !facilities.contains(["Level":"HOS\(level)", "Location":name])
//                        {
//                            facilities.append(["Level":"HOS\(level)", "Location":name])
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    func getFacilityByHotLevel(lvlArr:[Int])
//    {
//        for level in lvlArr
//        {
//            //get the documents directory url
//            let vrFPHpath = "VR/OFH/\(level)"
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(vrFPHpath)
//            if FileManager.default.fileExists(atPath: documentsDirectory.path)
//            {
//                let directoryContents = try? FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//                let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//                let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//                for name in subdirNamesStr
//                {
//                    facilities.append(["Level":"HOT\(level)", "Location":name])
//                }
//            }
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: vrFPHpath, withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                if FileManager.default.fileExists(atPath: infoUrl.path)
//                {
//                    let directoryContents = try? FileManager.default.contentsOfDirectory(at: infoURL!, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: [])
//                    let subdirs = directoryContents!.filter{ $0.hasDirectoryPath }
//                    let subdirNamesStr = subdirs.map{ $0.lastPathComponent }
//                    for name in subdirNamesStr
//                    {
//                        if !facilities.contains(["Level":"HOT\(level)", "Location":name])
//                        {
//                            facilities.append(["Level":"HOT\(level)", "Location":name])
//                        }
//                    }
//                }
//            }
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showER"
        {
            
            let destination = segue.destination as! VRViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            if indexPath != nil
            {
                let level = facilities2[buildingChosen]![indexPath!.section].key
                deselectAllLevelButtons()
                for button in buildingLevelsButtonArray
                {
                    if button.titleLabel?.text == "\(level)"
                    {
                        button.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
                        button.layer.borderColor = UIColor.white.cgColor
                        button.setTitleColor(.white, for: .normal)
                        //print ("\(level)")
                    }
                }
                
                let chosenFac = self.tableView.cellForRow(at: indexPath!)?.textLabel?.text
                
                buildingLevelChosen = Int(String(tableView.headerView(forSection: indexPath!.section)!.textLabel!.text!.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()))!
                
                destination.chosenFac = chosenFac!
                destination.chosenLvl = "\(buildingLevelChosen)"
                if buildingChosen == "HOS"
                {
                    destination.chosenBuilding = "FPH"
                }
                if buildingChosen == "HOT"
                {
                    destination.chosenBuilding = "OFH"
                }
            }
        }
        
        if segue.identifier == "showHome"
        {
            showFlower = true
            let destination = segue.destination as! coverPageViewController
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //Yusuf:This is for the flashing of the scrollbar
        tableView.flashScrollIndicators()
    }
}
