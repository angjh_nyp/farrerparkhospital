//
//  AboutUsViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 3/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation

class AboutUsViewController: UIViewController {
    
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBAction func homeBtnPressed(_ sender: UIButton) {
        menu.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var aboutus: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
//    @IBOutlet weak var contentLbl: UITextView!

    @IBOutlet weak var contentLbl: UITextView!
    //@IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var versionLbl: UILabel!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTnC"
        {
            let dest = segue.destination as! TermsConditionViewController
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        versionLbl.isHidden = true
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
        menu.currVC = self
        //menu.alertPop = menu.createCSpop(view: view, blurredScreen: blurredScreen, vc: self)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //scrollView.contentSize = stackView.frame.size
        scrollView.flashScrollIndicators()
        
        contentLbl.isEditable = false
        contentLbl.dataDetectorTypes = UIDataDetectorTypes.all;
        
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        
        aboutus.image = UIImage(named: AboutUsDetails().aboutUsImage)
        
        let descLbl = NSString(format: AboutUsDetails().htmlStyle as NSString, "\(AboutUsDetails().description)") as String
        let htmlWriteUp = try? NSMutableAttributedString(data: Data(descLbl.utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        contentLbl.attributedText = htmlWriteUp
        
        //contentLbl.text = "testing"
        titleLbl.text = AboutUsDetails().title
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //Yusuf:This is for the flashing of the scrollbar
        contentLbl.flashScrollIndicators()
    }
    
    @objc func canRotate() -> Void {}

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (self.isMovingFromParent) {
          UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
        }

    }
    
    
//    // Set the shouldAutorotate to False
//    override open var shouldAutorotate: Bool {
//       return false
//    }
//
//    // Specify the orientation.
//    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//       return .portrait
//    }
}
