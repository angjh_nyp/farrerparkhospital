//
//  GadgetInfoViewController.swift
//  FarrerParkHospital
//
//  Created by angjh on 25/3/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation
import UIKit

class GadgetInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let menu = flowerMenu()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var scanBtn: UIButton!
    var gadgetNameArr: [String] = []
    var OFHgadgets: [String] = []
    var FPHgadgets: [String] = []
    var FPHgadgetImages: [UIImage] = []
    var OFHgadgetImages: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.dataSource = self
        tableView.delegate = self
        retrieveGadgetNames()
        getImagesFromFolders()
        //tableView.register(CustomGadgetCell.self, forCellReuseIdentifier: "gadgetCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        menu.currVC = self
        
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        versionLbl.isHidden = true
        
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.flashScrollIndicators()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGadgetAR"
        {
            let _ = segue.destination as! GadgetARViewController
        }
    }
    
    @IBAction func homeBtnPressed(_ sender: UIButton) {
        menu.animateBtn(homeBtn: sender, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return OFHgadgets.count
        }
        else {
            return FPHgadgets.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "One Farrer Hotel"
        }
        else {
            return "Farrer Park Hospital"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gadgetCell", for: indexPath) as! CustomGadgetCell
        var name: String
        if indexPath.section == 1 {
            name = OFHgadgets[indexPath.row]
            cell.gadgetImage.image = OFHgadgetImages[indexPath.row]
            cell.gadgetImage.contentMode = .scaleAspectFit
            cell.gadgetImage.backgroundColor = .darkGray
        }
        else {
            name = FPHgadgets[indexPath.row]
            cell.gadgetImage.image = FPHgadgetImages[indexPath.row]
            cell.gadgetImage.contentMode = .scaleAspectFit
            cell.gadgetImage.backgroundColor = .darkGray

        }
        cell.gadgetName.text = name
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        (view as! UITableViewHeaderFooterView).textLabel?.textAlignment = .center
    }
    
    //gadgetArr populated at coverpage
    func retrieveGadgetNames() {
        for gadget in gadgetArr {
            if gadget.entityType == "OFH" {
                OFHgadgets.append(gadget.name ?? "missing gadget name")
            }
            else {
                FPHgadgets.append(gadget.name ?? "missing gadget name")
            }
        }
    }
    
    func getImagesFromFolders() {

        for gad in gadgetArr
        {
            let ddfPath = "AR/\(gad.entityType!)/\(gad.level!)/\(gad.name!)"
            let gadgetDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(ddfPath)
            
            if FileManager.default.fileExists(atPath: gadgetDir.path) {
                let arImageDir = gadgetDir.appendingPathComponent("baseImage.jpg")
                let imageData = try? Data(contentsOf: arImageDir)
                let image = UIImage(data: imageData!)
                if gad.entityType == "OFH"
                {
                    OFHgadgetImages.append(image!)
                }
                else
                {
                    FPHgadgetImages.append(image!)
                }
            }
            else {
                guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
                guard let bundle = Bundle(url: bundleURL) else { return }
                let infoURL = bundle.url(forResource: ddfPath, withExtension: nil)
                if let infoUrl = infoURL
                {
                    if FileManager.default.fileExists(atPath: infoUrl.path)
                    {
                        let imageDir = infoUrl.appendingPathComponent("baseImage.jpg")
                        let imageData = try? Data(contentsOf: imageDir)
                        let image = UIImage(data: imageData!)
                        if gad.entityType == "OFH"
                        {
                            OFHgadgetImages.append(image!)
                        }
                        else
                        {
                            FPHgadgetImages.append(image!)
                        }
                    }
                }
            }
        }
    }

}
