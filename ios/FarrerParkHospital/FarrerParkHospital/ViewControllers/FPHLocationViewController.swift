//
//  FPHLocationViewController.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 14/2/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FPHLocationViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func returnBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Set the centre of the map
        var location = CLLocationCoordinate2D()
        location.latitude = 1.3128
        location.longitude = 103.8540
        
        let camera = MKMapCamera()
        camera.altitude = 500
        camera.centerCoordinate = location

        //Set to the region with animated effect
        self.mapView.setCamera(camera, animated:true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = location
        pin.title = "Farrer Park Hospital"
        self.mapView.addAnnotation(pin)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
