//
//  GadgetARViewController.swift
//  FarrerParkHospital
//
//  Created by angjh on 25/3/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation
import UIKit
import ARKit

let documentsURL2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
let arUrl2 = documentsURL.appendingPathComponent("AR")
let vrUrl2 = documentsURL.appendingPathComponent("VR")
var finalDirURL2:URL?

class GadgetARViewController: UIViewController, ARSCNViewDelegate {
    
    var locationFromHamburger:String = ""
    var levelFromHamburger = ""
    @IBOutlet weak var sceneView: ARSCNView!
    private var worldConfiguration: ARWorldTrackingConfiguration?
    @IBOutlet weak var backBtn: bigCircleBtn!
    let menu = flowerMenu()
    @IBOutlet weak var blurredScreen: ViewQualities!
    @IBOutlet weak var homeBtn: bigCircleBtn!
    @IBOutlet weak var versionLbl: UILabel!
    var currentNode:SCNNode?
    var descNode:SCNNode?
    var currArType:Int?
    var overlayNode:SCNNode?
    var anchorObj:ARImageAnchor?
    var anchorID:UUID?
    
    @IBAction func homeBtnPressed(_ sender: UIButton) {
        menu.animateBtn(homeBtn: homeBtn, blurredScreen: blurredScreen, view: view, versionLbl: versionLbl, rtnBtn: backBtn)
    }
    
    @IBAction func returnBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene
        
        getImagesFromFolders()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        menu.orientationChange(view: size)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        versionLbl.isHidden = true
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionLbl.text = "App Version \(text)"
        }
        
        if let configuration = worldConfiguration {
            sceneView.session.run(configuration)
        }
        
        menu.currVC = self
        if !nextVR
        {
            menu.createFlowerMenu(view: self.view, navigator: self.navigationController!)
        } else {
            nextVR = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        
        if (self.isMovingFromParent) {
            UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
          }
        
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    @objc func canRotate() -> Void {}

    func getImagesFromFolders() {
        
        //For AR Images
        worldConfiguration = ARWorldTrackingConfiguration()
        guard var referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Images", bundle: nil) else {
            fatalError("Missing expected asset catalog resources.")
        }
        
        referenceImages.removeAll()
        
        //get folder contents from local directory
        let arFolderContents = try? FileManager.default.contentsOfDirectory(at: arUrl2, includingPropertiesForKeys: nil)
        let vrFolderContents = try? FileManager.default.contentsOfDirectory(at: vrUrl2, includingPropertiesForKeys: nil)
        
//        let test = documentsURL.appendingPathComponent("AR/FPH/1/Television Remote Control/arImage.jpg")
//        if FileManager.default.fileExists(atPath: test.path)
//        {
//            print("YES")
//        }
        
//        if FileManager.default.fileExists(atPath: arUrl.path)
//        {
//            finalDirURL = arUrl
//        } else {
//            guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
//            guard let bundle = Bundle(url: bundleURL) else { return }
//            let infoURL = bundle.url(forResource: "AR", withExtension: nil)
//            if let infoUrl = infoURL
//            {
//                finalDirURL = infoUrl
//            }
//        }
//
//        guard let finalURL = finalDirURL else { return }
        
        
        for gad in gadgetArr
        {
            let ddfPath = "AR/\(gad.entityType!)/\(gad.level!)/\(gad.name!)"
            let gadgetDir = documentsURL2.appendingPathComponent(ddfPath)
            if FileManager.default.fileExists(atPath: gadgetDir.path){
                let arImageDir = gadgetDir.appendingPathComponent("baseImage.jpg")
                let imageData = try? Data(contentsOf: arImageDir)
                let image = UIImage(data: imageData!)
                let cgimage = (image?.cgImage)!
                
                let plistDir = gadgetDir.appendingPathComponent("Info.plist")
                let plistXML = FileManager.default.contents(atPath: plistDir.path)
                let plistData = try? PropertyListSerialization.propertyList(from: plistXML!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                var plistBaseImageHeight = plistData!["baseImageDimensionHeight"]!
                var plistBaseImageWidth = plistData!["baseImageDimensionWidth"]!
                
                //when physical width is actual, then use plistBaseImageWidth
                //hardcoded width = 0.2
                let ARImages = ARReferenceImage(cgimage, orientation: CGImagePropertyOrientation.up, physicalWidth: plistBaseImageWidth as! CGFloat)
                ARImages.name = "\(gadgetDir)"
                referenceImages.insert(ARImages)
            } else {
                guard let bundleURL = Bundle.main.url(forAuxiliaryExecutable: "Picture") else { return }
                guard let bundle = Bundle(url: bundleURL) else { return }
                let infoURL = bundle.url(forResource: ddfPath, withExtension: nil)
                if let infoUrl = infoURL
                {
                    if FileManager.default.fileExists(atPath: infoUrl.path)
                    {
                        let imageDir = infoUrl.appendingPathComponent("baseImage.jpg")
                        
                        let imageData = try? Data(contentsOf: imageDir)
                        let image = UIImage(data: imageData!)
                        let cgimage = (image?.cgImage)!
                        
                        let plistDir = infoUrl.appendingPathComponent("Info.plist")
                        let plistXML = FileManager.default.contents(atPath: plistDir.path)
                        let plistData = try? PropertyListSerialization.propertyList(from: plistXML!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                        var plistBaseImageHeight = plistData!["baseImageDimensionHeight"]!
                        var plistBaseImageWidth = plistData!["baseImageDimensionWidth"]!
                        
                        //when physical width is actual, then use plistBaseImageWidth
                        //hardcoded width = 0.2
                        let ARImages = ARReferenceImage(cgimage, orientation: CGImagePropertyOrientation.up, physicalWidth: plistBaseImageWidth as! CGFloat)
                        ARImages.name = "\(infoUrl)"
                        referenceImages.insert(ARImages)
                    }
                }
            }
        }
        
        worldConfiguration?.detectionImages = referenceImages
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if let imageAnchor = anchor as? ARImageAnchor {
            
            if let ancID = anchorID
            {
                if ancID != imageAnchor.identifier
                {
                    currentNode!.removeFromParentNode()
                    self.sceneView.session.remove(anchor: anchorObj!)
                    if let currAT = currArType
                    {
                        if currAT == 1
                        {
                            descNode?.removeFromParentNode()
                            
                        } else if currAT == 2
                        {
                            overlayNode?.removeFromParentNode()
                        }
                    }
                    anchorObj = imageAnchor
                    anchorID = imageAnchor.identifier
                    currentNode = node
                }
            } else {
                currentNode = node
                anchorID = imageAnchor.identifier
                anchorObj = imageAnchor
            }
            
            handleFoundImage(imageAnchor, node)
            
        }
    }
    
    private func handleFoundImage(_ imageAnchor: ARImageAnchor, _ node: SCNNode) {
        
        let finalUrl = imageAnchor.name
        
        //let imageFolder = finalUrl.appendingPathComponent(String(anchorNameSubstr!))
        if finalUrl != nil
        {
            let imageFolder = URL(string: finalUrl!)
            if imageFolder != nil
            {
                let plistDir = imageFolder!.appendingPathComponent("Info.plist")
                let plistXML = FileManager.default.contents(atPath: plistDir.path)
                let plistData = try? PropertyListSerialization.propertyList(from: plistXML!, options: .mutableContainersAndLeaves, format: nil) as! [String:Any]
                let plistARType = plistData!["arType"] as! Int
                let plistOffsetDataY = plistData!["arOffsetY"] as! CGFloat
                let plistOffsetDataX = plistData!["arOffsetX"] as! CGFloat
                let plistOffsetDataZ = plistData!["arOffsetZ"] as! CGFloat
                let screenBounds = UIScreen.main.bounds
                
                let overlayPlane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width * 1.5, height: imageAnchor.referenceImage.physicalSize.height * 1.5)
                let overlayPlaneNode = SCNNode(geometry: overlayPlane)
                
                let displaySceneSize = CGSize(width: screenBounds.width, height: screenBounds.height)
                let displayScene = SKScene(size: displaySceneSize)
                displayScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)

                let descriptionPlane = SCNPlane(width: screenBounds.width / 2500, height: screenBounds.height / 7500)
                let descriptionPlaneNode = SCNNode(geometry: descriptionPlane)
                let constraint = SCNBillboardConstraint()
                constraint.freeAxes = SCNBillboardAxis.Y
                if plistARType == 1 {
                    
                    let fullDesc = NSMutableAttributedString()
                    
                    let titleLabel = "\(plistData!["name"] as! String)\n"
                    let titleAttr = [ NSMutableAttributedString.Key.font: UIFont(name: "Titillium-Semibold", size: 65), NSAttributedString.Key.foregroundColor :UIColor(red: 163/255, green: 32/255, blue: 55/255, alpha: 1.0)]
                    let titleString = NSAttributedString(string: titleLabel, attributes: titleAttr)
                    
                    let descLbl = NSString(format: "<span style=\'font-family: Titillium-Semibold; font-size: 15\'>%@</span>" as NSString, "\(plistData!["desc"] as! String)") as! String
                    let testStr = try? NSMutableAttributedString(data: Data(descLbl.utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
                    
                    guard let descString = testStr else { return }
                    descString.addAttribute(NSMutableAttributedString.Key.font, value: UIFont(name: "Titillium-Semibold", size: 30), range: NSMakeRange(0, descString.length))
                    
                    fullDesc.append(titleString)
                    fullDesc.append(descString)
                    
                    print("AR Found: \(titleLabel)")
                    
                    let labelNode = SKLabelNode(attributedText: fullDesc)
                    labelNode.lineBreakMode = NSLineBreakMode.byWordWrapping
                    labelNode.numberOfLines = 0
                    labelNode.preferredMaxLayoutWidth = screenBounds.width
                    labelNode.horizontalAlignmentMode = .center
                    labelNode.verticalAlignmentMode = .center
                    
                    
                    let descriptionSceneSize = CGSize(width: screenBounds.width + 50, height: labelNode.frame.height + 50)
                    let descriptionScene = SKScene(size: descriptionSceneSize)
                    descriptionScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                    descriptionScene.backgroundColor = UIColor.white
                    descriptionScene.addChild(labelNode)
                    //descriptionScene.addChild(labelNode)
                    descriptionPlane.cornerRadius = descriptionPlane.width * 0.02
                    descriptionPlane.firstMaterial?.diffuse.contents = descriptionScene
                    descriptionPlane.firstMaterial?.isDoubleSided = true
                    descriptionPlane.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)
                    descriptionPlaneNode.position = SCNVector3(plistOffsetDataX, plistOffsetDataY, plistOffsetDataZ)
                    //descriptionPlaneNode.position = SCNVector3(0.0, 0.0, -0.1)
                    descriptionPlaneNode.constraints = [constraint]
                    
                    currArType = 1
                    node.addChildNode(descriptionPlaneNode)
                    descNode = descriptionPlaneNode
                }
                else if plistARType == 2 {
                    let imageURL = imageFolder!.appendingPathComponent("arImage.jpg")
                    let imageData = try? Data(contentsOf: imageURL)
                    let displayUIImage = UIImage(data: imageData!)
                    let displayTexture = SKTexture(image: displayUIImage!)
                    let displaySprite = SKSpriteNode(texture: displayTexture)
        //                displaySprite.size.width = displayScene.size.width
        //                displaySprite.size.height = displayScene.size.height
                    displayScene.backgroundColor = UIColor.clear
                    displayScene.addChild(displaySprite)
                    overlayPlane.firstMaterial?.diffuse.contents = displayScene
                    overlayPlane.firstMaterial?.isDoubleSided = true
                    overlayPlane.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0)
                    overlayPlaneNode.position = SCNVector3(plistOffsetDataX, plistOffsetDataY, plistOffsetDataZ)
                    overlayPlaneNode.constraints = [constraint]
                    currArType = 2
                    node.addChildNode(overlayPlaneNode)
                    overlayNode = overlayPlaneNode
                }
                else if plistARType == 3 {
                    
                }
            }
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }

}
