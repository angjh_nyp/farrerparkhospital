//
//  FACILITY.swift
//  projProto
//
//  Created by FYPJ on 11/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import Foundation

class FACILITY: NSObject {
    var id:String = "id"
    var name:String = "name"
    var floorLevel:String = "floorLevel"
    var entityType:String = "entityType"
    var lastModified:String = "lastModified"
    var VRImage:String = "VRImage"
    var hotspots:String = "hotspots"
    var thumbNail:String = "thumbNail"
    var priority:String = "priority"
}
