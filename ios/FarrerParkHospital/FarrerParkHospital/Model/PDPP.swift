//
//  PDPP.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 4/2/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation

class PDPP
{
    var htmlStyle = "<span style=\'font-family: Titillium-Regular; font-size: 17\'>%@</span>"
    var title:String = "Personal Data Protection Policies"
    var description:String = "<p><strong><u>Personal Data Protection Act </u></strong></p><p>You hereby acknowledge and agree that the App and App Functions may use transmissions over the Internet which are never completely private or secure and that any information transmitted to the App may not be confidential and acknowledge that you have no expectation of privacy with respect to such information, subject always to the App&rsquo;s Data Protection Policy.</p><p>We are committed to data protection. Personal information which you provide to us is subject to the App&rsquo;s Data Protection Policy. [Suggest to insert the link for Personal Data Protection Policy of FPH here]</p><p>If you have questions or concerns regarding your personal data or any aspect of the Data Protection Policy, please contact us at <a href=\"mailto:pdpa@farrerpark.com\">pdpa@farrerpark.com</a>.</p>"
}
