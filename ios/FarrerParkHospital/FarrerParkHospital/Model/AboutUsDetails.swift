//
//  AboutUsDetails.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 4/2/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation

class AboutUsDetails
{
    var aboutUsImage:String = "newestAboutUs"
    var htmlStyle = "<span style=\'font-family: Titillium-Regular; text-align: justify;font-size: 17\'>%@</span>"
    var title:String = "ABOUT US"
    var description:String = "<h3 class=\"mb-4\">Farrer Park Hospital is a private tertiary healthcare institute set up to offer a fresh approach to medical treatment.</h3><p>Our care philosophy extends beyond healing and the management of disease to engaging our patients as partners in the pursuit of good health and providing an oasis for realising these goals.</p><p><em>Attention has been given to every aspect and detail of the facility, from the comfort of our patients, to its impact on the environment, to the speed and ease of obtaining medical attention, to the maintenance of hygiene.</em></p><p>Current developments in medical technology and treatments have been integrated with the hospital design and architecture, all determined by doctors to enhance our services and better realise our duty to our patients.</p><p>The hospital, which has a medical centre attached, is also part of a lifestyle concept which combines healthcare and hospitality. It is set in a 20-storey building &ndash; Connexion &ndash; which houses a hotel, a spa and Owen Link.</p><p>The complex sits on top of the Farrer Park MRT station, in an area rich in history. Read more about the&nbsp;<a        href=\"https://www.farrerpark.com/about-us/Why-Farrer-Park-Hospital/Farrer-Park-Location-History.html\">history of Farrer Park</a>.</p><p>At Farrer Park Hospital, unhurried, comprehensive evaluations offer the chance of healing and getting back to your life.&nbsp;Our specialists collaborate across disciplines to listen to your story, evaluate your condition from every angle, and develop a diagnosis and treatment plan that's just for you.&nbsp;Current developments in medical technology, treatments and innovations allow our patients modern and efficient healthcare services.</p><h4 class=\"col-4\"><em>Our Vision, Mission and Values</em></h4><div class=\"row d-flex flex-row align-items-center\"><h4 class=\"col-7 ml-auto\"><em>To bring value to people through fresh and fair solutions for healthcare, hospitality and wellness.</em></h4></div>"
}
