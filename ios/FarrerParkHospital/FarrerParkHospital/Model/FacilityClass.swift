//
//  FacilityClass.swift
//  projProto
//
//  Created by FYPJ on 5/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import UIKit

class FacilityClass: NSObject {
    var id:String?
    var name:String?
    var floorlevel:String?
    var entityType:String?
    var desc: String?
    var timeStamp:String?
    var vrImageLink:String?
    var hotspotsArr:[String]?
    var hotspotsName:[String]?
    var vrThumbNail:String?
    var hotspotsThumb:[String:String]?
    var priority:String?
    init(id:String, name:String, floorlevel:String, entityType:String, desc:String, timeStamp:String, vrImageLink:String, hotspotsArr: [String], hotspotsName:[String], vrThumbNail:String, hotspotsThumb:[String:String], priority:String) {
        self.id = id
        self.name = name
        self.floorlevel = floorlevel
        self.entityType = entityType
        self.desc = desc
        self.timeStamp = timeStamp
        self.vrImageLink = vrImageLink
        self.hotspotsArr = hotspotsArr
        self.hotspotsName = hotspotsName
        self.vrThumbNail = vrThumbNail
        self.hotspotsThumb = hotspotsThumb
        self.priority = priority
    }
}
