//
//  Constant.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 31/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

struct Constant {
    static let NORMAL_NODE_NAME: String = "node"
    static let DESTINATION_NODE_NAME: String = "dest"
    static let FIRST_NODE_NAME: String = "first"
    static let IMAGE_ANCHOR_NAME: String = "anchor"
    static let TEXT_NODE_NAME: String = "text"
}
