//
//  Point.swift
//  FarrerParkHospital
//
//  Created by FYPJ on 31/1/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation
import SceneKit

class Point: NSObject {
    var type: String?
    var coordinate: SCNVector3?
    var text: String?
}
