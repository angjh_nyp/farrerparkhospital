//
//  GADGET.swift
//  projProto
//
//  Created by FYPJ on 16/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import Foundation

class GADGET: NSObject {
    var id:String = "id"
    var name:String = "name"
    var level:String = "level"
    var entityType:String = "entityType"
    var desc: String = "desc"
    var timeStamp:String = "lastModified"
    var arImageLink:String = "arImage"
    var baseImage:String = "baseImage"
    var arType:String = "arType"
    var baseImageDimHeight:String = "baseImageDimensionHeight"
    var baseImageDimWidth:String = "baseImageDimensionWidth"
    var arOffsetY:String = "arOffsetY"
    var arOffsetZ:String = "arOffsetZ"
    var arOffsetX:String = "arOffsetX"
}
