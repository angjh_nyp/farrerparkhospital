//
//  GadgetClass.swift
//  projProto
//
//  Created by FYPJ on 13/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import Foundation

class GadgetClass: NSObject {
    var id:String?
    var name:String?
    var level:String?
    var entityType:String?
    var desc: String?
    var timeStamp:String?
    var arImageLink:String?
    var baseImage:String?
    var arType:Int?
    var baseImageDimHeight:Double?
    var baseImageDimWidth:Double?
    var arOffsetY:Double?
    var arOffsetZ:Double?
    var arOffsetX:Double?
    init(id:String, name:String, level:String, entityType:String, desc:String, timeStamp:String, arImageLink:String, baseImage:String, arType:Int, baseImageDimHeight:Double, baseImageDimWidth:Double, arOffsetY:Double, arOffsetZ:Double, arOffsetX:Double) {
        self.id = id
        self.name = name
        self.level = level
        self.entityType = entityType
        self.desc = desc
        self.timeStamp = timeStamp
        self.arImageLink = arImageLink
        self.baseImage = baseImage
        self.arType = arType
        self.baseImageDimHeight = baseImageDimHeight
        self.baseImageDimWidth = baseImageDimWidth
        self.arOffsetY = arOffsetY
        self.arOffsetZ = arOffsetZ
        self.arOffsetX = arOffsetX
    }
}
