//
//  ViewQualities.swift
//  projProto
//
//  Created by FYPJ on 17/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import UIKit
@IBDesignable

class ViewQualities: UIView {

    @IBInspectable var opacity: Float {
         get {
            return Float(self.layer.opacity)
         }set {
            self.layer.opacity = newValue
         }
    }

}
