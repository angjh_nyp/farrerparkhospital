//
//  ButtonCircular.swift
//  projProto
//
//  Created by FYPJ on 28/11/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import UIKit
@IBDesignable

class ButtonCircular: UIButton {

    //@IBInspectable variables are exposed to the Storyboard U
    @IBInspectable var cornerRadius: Double {
         get {
           return Double(self.layer.cornerRadius)
         }set {
           self.layer.cornerRadius = CGFloat(newValue)
         }
    }
    
    @IBInspectable var clipBounds: Bool = true {
        didSet{
            self.clipsToBounds = self.clipBounds
        }
    }

}
