//
//  CustomGadgetCell.swift
//  FarrerParkHospital
//
//  Created by angjh on 31/3/20.
//  Copyright © 2020 FYPJ. All rights reserved.
//

import Foundation
import UIKit

class CustomGadgetCell: UITableViewCell {
    
    @IBOutlet weak var gadgetImage: UIImageView!
    @IBOutlet weak var gadgetName: UILabel!
    
}
