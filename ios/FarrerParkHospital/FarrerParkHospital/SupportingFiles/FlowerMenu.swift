//
//  FlowerMenu.swift
//  projProto
//
//  Created by FYPJ on 26/12/19.
//  Copyright © 2019 FYPJ. All rights reserved.
//

import Foundation

//change value of this optional label when entering another view controller
var vrErrLblGbl: UILabel?
var nextVR:Bool = false

public class flowerMenu: NSObject {
    
    var selectLocBtn:UIButton?
    var wayFindBtn:UIButton?
    var patJourBtn:UIButton?
    var expRealBtn:UIButton?
    var aboutUsBtn:UIButton?
    var conUsBtn:UIButton?
    
    var slCenter: CGPoint!
    var wfCenter: CGPoint!
    var pjCenter: CGPoint!
    var erCenter: CGPoint!
    var auCenter: CGPoint!
    var cuCenter: CGPoint!
    var homeCenter: CGPoint!
    var conView:UIView?
    var alertPop:UIAlertController?
    var currVC:UIViewController?
    
    var navigator: UINavigationController?
    
    func flowerBtn(bgImage: UIImage, btnImage: UIImage) -> UIButton {
        let button = UIButton()
        button.setBackgroundImage(bgImage, for: .normal)
        button.setImage(btnImage, for: .normal)
        button.frame.size = CGSize(width: 85, height: 85)
        button.layer.cornerRadius = 42
        button.clipsToBounds = true
        return button
    }
    
    func createFlowerMenu(view: UIView, navigator: UINavigationController)
    {
        let slBtn = flowerBtn(bgImage: UIImage(named: "btn_location512")!, btnImage: UIImage(named: "btn_location512")!)
        slBtn.layer.position = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        slBtn.addTarget(self, action: #selector(goToSelectPg), for: .touchUpInside)
        view.addSubview(slBtn)
        let pjBtn = flowerBtn(bgImage: UIImage(named: "btn_patientjourney512")!, btnImage: UIImage(named: "btn_patientjourney512")!)
        pjBtn.layer.position = CGPoint(x: view.frame.width/2, y: view.frame.height/2 - slBtn.frame.height - 20)
        pjBtn.addTarget(self, action: #selector(goToPatJourPg), for: .touchUpInside)
        view.addSubview(pjBtn)
        let wfBtn = flowerBtn(bgImage: UIImage(named: "btn_wayfinder512")!, btnImage: UIImage(named: "btn_wayfinder512")!)
        wfBtn.layer.position = CGPoint(x: view.frame.width/2 + ((slBtn.frame.width + 20)*(slBtn.frame.width + 20)/11*10).squareRoot(), y: view.frame.height/2 - ((slBtn.frame.height + 20)*(slBtn.frame.height + 20)/11).squareRoot())
        wfBtn.addTarget(self, action: #selector(goToWayFindPg), for: .touchUpInside)
        view.addSubview(wfBtn)
        let erBtn = flowerBtn(bgImage: UIImage(named: "btn_reality512")!, btnImage: UIImage(named: "btn_reality512")!)
        erBtn.layer.position = CGPoint(x: view.frame.width/2 - ((slBtn.frame.width + 20)*(slBtn.frame.width + 20)/11*10).squareRoot(), y: view.frame.height/2 - ((slBtn.frame.height + 20)*(slBtn.frame.height + 20)/11).squareRoot())
        erBtn.addTarget(self, action: #selector(goToVr), for: .touchUpInside)
        view.addSubview(erBtn)
        let auBtn = flowerBtn(bgImage: UIImage(named: "btn_aboutus512")!, btnImage: UIImage(named: "btn_aboutus512")!)
        auBtn.layer.position = CGPoint(x: view.frame.width/2 - ((slBtn.frame.width + 20)*(slBtn.frame.width + 20)/11*4).squareRoot(), y: view.frame.height/2 + ((slBtn.frame.height + 20)*(slBtn.frame.height + 20)/11*7).squareRoot())
        auBtn.addTarget(self, action: #selector(goToAboutUsPg), for: .touchUpInside)
        view.addSubview(auBtn)
        let cuBtn = flowerBtn(bgImage: UIImage(named: "btn_contactus512")!, btnImage: UIImage(named: "btn_contactus512")!)
        cuBtn.layer.position = CGPoint(x: view.frame.width/2 + ((slBtn.frame.width + 20)*(slBtn.frame.width + 20)/11*4).squareRoot(), y: view.frame.height/2 + ((slBtn.frame.height + 20)*(slBtn.frame.height + 20)/11*7).squareRoot())
        cuBtn.addTarget(self, action: #selector(gotToConUsPg), for: .touchUpInside)
        view.addSubview(cuBtn)
        
        self.selectLocBtn = slBtn
        self.wayFindBtn = wfBtn
        self.patJourBtn = pjBtn
        self.expRealBtn = erBtn
        self.aboutUsBtn = auBtn
        self.conUsBtn = cuBtn
        
        slCenter = self.selectLocBtn!.center
        wfCenter = self.wayFindBtn!.center
        pjCenter = self.patJourBtn!.center
        erCenter = self.expRealBtn!.center
        auCenter = self.aboutUsBtn!.center
        cuCenter = self.conUsBtn!.center
        
        self.selectLocBtn!.center = self.selectLocBtn!.center
        self.wayFindBtn!.center = self.selectLocBtn!.center
        self.patJourBtn!.center = self.selectLocBtn!.center
        self.expRealBtn!.center = self.selectLocBtn!.center
        self.aboutUsBtn!.center = self.selectLocBtn!.center
        self.conUsBtn!.center = self.selectLocBtn!.center
        
        self.selectLocBtn!.isHidden = true
        self.wayFindBtn!.isHidden = true
        self.patJourBtn!.isHidden = true
        self.expRealBtn!.isHidden = true
        self.aboutUsBtn!.isHidden = true
        self.conUsBtn!.isHidden = true
        
        self.navigator = navigator
    }
    
    func contactBtn() -> UIButton {
        let button = UIButton()
        button.setTitle("Contact", for: .normal)
        button.addTarget(self, action: #selector(contactPressed), for: .touchUpInside)
        return button
    }
    
    func cancelBtn() -> UIButton {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.addTarget(self, action: #selector(contactClosed), for: .touchUpInside)
        return button
    }
    
//    func createCSpop(view:UIView, blurredScreen:UIView, vc:UIViewController) -> UIAlertController
//    {
//        let csPop = UIView()
//        csPop.frame.size.height = 150
//        csPop.frame.size.width = 278
//        csPop.backgroundColor = UIColor.white
//        let contact = contactBtn()
//        contact.frame.size = CGSize(width: csPop.frame.size.width, height: 20)
//        contact.setTitleColor(view.tintColor, for: .normal)
//        let cancel = cancelBtn()
//        cancel.frame.size = CGSize(width: csPop.frame.size.width, height: 20)
//        cancel.setTitleColor(view.tintColor, for: .normal)
//        view.addSubview(csPop)
//        csPop.isHidden = true
//        csPop.center = view.center
//        csPop.alpha = 1
//        csPop.transform = CGAffineTransform(scaleX: 1.2, y: 1.6)
//        
//        let alert = UIAlertController(title: "Call Farrer Park?\n67052999", message: nil, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Call", style: .default, handler: {
//            _ in
//            let number = URL(string: "tel://83998785")
//            UIApplication.shared.open(number!, options: [:], completionHandler: nil)
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in}))
//        currVC = vc
//        
//
//        let stack = UIStackView(arrangedSubviews: [contact, cancel])
//        stack.axis = .vertical
//        stack.distribution = .fillEqually
//        
//        csPop.addSubview(stack)
//        stack.translatesAutoresizingMaskIntoConstraints = false
//        let trailingStack = NSLayoutConstraint(item: stack, attribute: .trailing, relatedBy: .equal, toItem: csPop, attribute: .trailing, multiplier: 1.0, constant: 0)
//        let leadingStack = NSLayoutConstraint(item: stack, attribute: .leading, relatedBy: .equal, toItem: csPop, attribute: .leading, multiplier: 1.0, constant: 0)
//        let bottomStack = NSLayoutConstraint(item: stack, attribute: .bottom, relatedBy: .equal, toItem: csPop, attribute: .bottom, multiplier: 1.0, constant: 0)
//        let topStack = NSLayoutConstraint(item: stack, attribute: .top, relatedBy: .equal, toItem: csPop, attribute: .top, multiplier: 1.0, constant: 0)
//        csPop.addConstraints([trailingStack, leadingStack, bottomStack, topStack])
//        
//        csPop.addSubview(cancel)
//        return alert
//    }
    
    @objc func contactPressed(sender: UIButton)
    {
        let number = URL(string: "tel://83998785")
        UIApplication.shared.open(number!, options: [:], completionHandler: nil)
    }
    
    @objc func contactClosed(sender: UIButton)
    {
        conView!.isHidden = true
    }
    
    @objc func goToSelectPg()
    {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "LocViewCtrl") as! LocationViewController
        self.navigator!.pushViewController(vc, animated: true)
    }
    
    @objc func goToPatJourPg()
    {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "PJCtrl") as! PJDropDownViewController
        self.navigator!.pushViewController(vc, animated: true)
    }
    
    @objc func goToWayFindPg()
    {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "WFCtrl") as! WayFinderViewController
        self.navigator!.pushViewController(vc, animated: true)
    }
    
    @objc func goToVr()
    {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "GadgetInfoController") as! GadgetInfoViewController
        self.navigator!.pushViewController(vc, animated: true)
//        if chosenFacGlobal != nil && chosenLvlGlobal != nil && chosenBuildGlobal != nil
//        {
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VRctrl") as! VRViewController
//            self.navigator!.pushViewController(vc, animated: true)
//        }
//        else {
////            if let vrHomeLbl = vrErrLblGbl
////            {
////                vrHomeLbl.text = "Select Location to experience reality"
////            }
//            let imgTitle = UIImage(named:"btn_reality512")
//            let imgViewTitle = UIImageView(frame: CGRect(x: 20, y: 15, width: 25, height: 25))
//            imgViewTitle.image = imgTitle
//
//            let alert = UIAlertController(title: "Experience Reality", message: "Select Location to experience reality", preferredStyle: .alert)
//            alert.view.addSubview(imgViewTitle)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//            currVC?.present(alert, animated: true, completion: nil)
//        }
    }
    
    @objc func goToAboutUsPg()
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AUctrl") as! AboutUsViewController
        self.navigator!.pushViewController(vc, animated: true)
    }
    
    @objc func gotToConUsPg(sender: UIButton)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cuCtrl") as! ContactUsViewController
        self.navigator!.pushViewController(vc, animated: true)
    }
    
    func animateBtn(homeBtn: UIButton, blurredScreen: ViewQualities, view: UIView, versionLbl: UILabel, rtnBtn:UIButton?)
    {
        homeBtn.layer.removeAllAnimations()
        
        if selectLocBtn!.isHidden
        {
            let returnImage = UIImage(named: "btn_return512")
            homeBtn.setImage(returnImage!, for: .normal)
            homeBtn.setBackgroundImage(returnImage!, for: .normal)
            selectLocBtn!.isHidden = false
            wayFindBtn!.isHidden = false
            patJourBtn!.isHidden = false
            expRealBtn!.isHidden = false
            aboutUsBtn!.isHidden = false
            conUsBtn!.isHidden = false
            if let rtnBtn = rtnBtn
            {
                rtnBtn.isHidden = true
            }
        
            UIView.animate(withDuration: 0.6, animations: {
                blurredScreen.opacity = 0.6
                versionLbl.isHidden = false
                self.selectLocBtn!.layer.position = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
                self.patJourBtn!.layer.position = CGPoint(x: view.frame.width/2, y: view.frame.height/2 - self.selectLocBtn!.frame.height - 20)
                self.wayFindBtn!.layer.position = CGPoint(x: view.frame.width/2 + ((self.selectLocBtn!.frame.width + 20)*(self.selectLocBtn!.frame.width + 20)/11*10).squareRoot(), y: view.frame.height/2 - ((self.selectLocBtn!.frame.height + 20)*(self.selectLocBtn!.frame.height + 20)/11).squareRoot())
                self.expRealBtn!.layer.position = CGPoint(x: view.frame.width/2 - ((self.selectLocBtn!.frame.width + 20)*(self.selectLocBtn!.frame.width + 20)/11*10).squareRoot(), y: view.frame.height/2 - ((self.selectLocBtn!.frame.height + 20)*(self.selectLocBtn!.frame.height + 20)/11).squareRoot())
                self.aboutUsBtn!.layer.position = CGPoint(x: view.frame.width/2 - ((self.selectLocBtn!.frame.width + 20)*(self.selectLocBtn!.frame.width + 20)/11*4).squareRoot(), y: view.frame.height/2 + ((self.selectLocBtn!.frame.height + 20)*(self.selectLocBtn!.frame.height + 20)/11*7).squareRoot())
                self.conUsBtn!.layer.position = CGPoint(x: view.frame.width/2 + ((self.selectLocBtn!.frame.width + 20)*(self.selectLocBtn!.frame.width + 20)/11*4).squareRoot(), y: view.frame.height/2 + ((self.selectLocBtn!.frame.height + 20)*(self.selectLocBtn!.frame.height + 20)/11*7).squareRoot())
            })
        } else
        {
            let homeImage = UIImage(named: "btn_home512")
            homeBtn.setImage(homeImage!, for: .normal)
            homeBtn.setBackgroundImage(homeImage!, for: .normal)
            
            if let rtnBtn = rtnBtn
            {
                rtnBtn.isHidden = false
            }
            
            UIView.animate(withDuration: 0.6, animations: {
                blurredScreen.opacity = 0
                versionLbl.isHidden = true
                self.selectLocBtn!.center = self.selectLocBtn!.center
                self.wayFindBtn!.center = self.selectLocBtn!.center
                self.patJourBtn!.center = self.selectLocBtn!.center
                self.expRealBtn!.center = self.selectLocBtn!.center
                self.aboutUsBtn!.center = self.selectLocBtn!.center
                self.conUsBtn!.center = self.selectLocBtn!.center
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.selectLocBtn!.isHidden = true
                self.wayFindBtn!.isHidden = true
                self.patJourBtn!.isHidden = true
                self.expRealBtn!.isHidden = true
                self.aboutUsBtn!.isHidden = true
                self.conUsBtn!.isHidden = true
            })
        }
    }
    
    func orientationChange(view: CGSize)
    {
        //bug here !
        if (self.selectLocBtn == nil){
            return
        }
        
        if (!self.selectLocBtn!.isHidden)
        {
            self.selectLocBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
            self.patJourBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2 - self.selectLocBtn!.frame.height - 20)
            self.wayFindBtn!.layer.position = CGPoint(x: view.width/2 + ((selectLocBtn!.frame.width + 20)*(selectLocBtn!.frame.width + 20)/11*10).squareRoot(), y: view.height/2 - ((selectLocBtn!.frame.height + 20)*(selectLocBtn!.frame.height + 20)/11).squareRoot())
            self.expRealBtn!.layer.position = CGPoint(x: view.width/2 - ((selectLocBtn!.frame.width + 20)*(selectLocBtn!.frame.width + 20)/11*10).squareRoot(), y: view.height/2 - ((selectLocBtn!.frame.height + 20)*(selectLocBtn!.frame.height + 20)/11).squareRoot())
            self.aboutUsBtn!.layer.position = CGPoint(x: view.width/2 - ((selectLocBtn!.frame.width + 20)*(selectLocBtn!.frame.width + 20)/11*4).squareRoot(), y: view.height/2 + ((selectLocBtn!.frame.height + 20)*(selectLocBtn!.frame.height + 20)/11*7).squareRoot())
            self.conUsBtn!.layer.position = CGPoint(x: view.width/2 + ((selectLocBtn!.frame.width + 20)*(selectLocBtn!.frame.width + 20)/11*4).squareRoot(), y: view.height/2 + ((selectLocBtn!.frame.height + 20)*(selectLocBtn!.frame.height + 20)/11*7).squareRoot())
        } else {
            self.selectLocBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
            self.patJourBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
            self.wayFindBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
            self.expRealBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
            self.aboutUsBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
            self.conUsBtn!.layer.position = CGPoint(x: view.width/2, y: view.height/2)
        }
    }
    
}

